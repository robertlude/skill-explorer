# Skill Explorer

Skill Explorer is an interface for viewing and editing
Olive skill files.

## Installation

```sh
git clone git@bitbucket.org:robertlude/skill-explorer.git
cd skill-explorer
npm install
```

## Usage

```sh
npm start
```

Ensure you have `olive-skills` cloned somewhere on your
machine.

In the settings dialog that appears, ensure the path to
the `olive-skills` repository is filled in. Click save.
You're ready to go!

## Console

Please see `console.md` for documentation. This will be migrated into the app
itself soon.

## Feature Requests/Bug Reports

Please reach out to me directly on Slack, `@robertlude`

## Contributing

If you're daring enough, or already know `electron`/`webpack`/`babel`/`react`/`redux`,
this section is for you.

When editing list-like code, please keep it alphabetized.

### Adding a new action

#### 1. Create new action type

* Add constant to `./src/actionTypes.js`
* Export constant

#### 2. Update `./src/reducers`

* Add constant to appropriate main reducer file
* Create individual reducer in the appropriate directory
* Import new reducer in the appropriate main reducer file
* Add reducer to action map in appropriate main reducer file with constant as key
* Add any new state keys as necessary to main reducer file

#### 3. Update `./src/actions`

* Create new action creator in `./src/actions/`
* Import new action in `./src/actions.js`
* Export action in `./src/actions.js`

### Adding a component

Export a wrapped version of the component (using `wrap` from `c/wrap`).
This will provide easy access to React-Redux and Material UI style
injection. Even if you have no need for these, you should still wrap
your components in case `wrap` gains further functionality that applies
to all components equally.

`wrap(data = {})(component)`

`data` is an object with the following optional keys:

| Key            | Purpose                                                                     |
| -------------- | --------------------------------------------------------------------------- |
| `dispathProps` | Input to `react-redux`'s `connect` function's `mapDispatchToProps` argument |
| `stateProps`   | Input to `react-redux`'s `connect` function's `mapStateToProps` argument    |
| `styles`       | Input to `@material-ui`'s `withStyles` function                             |

`wrap(...)` produces a function that takes an argument, `component`, which is
the component you wish to wrap.

#### Example

```javascript
// External Dependencies

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

import someAction from '@/actions'

// Module

/* NOTE: These three constants should ideally live in their own submodules for
   code cleanliness. This is a very minimal example, typically these three
   parts contain more when and if they are used. None are required.
*/

const dispatchProps = { someAction }
    , stateProps    = state => ({ aStateProp: state.someValue })
    , styles        = theme => ({ aClassName: { padding: theme.spacing(1) }})

class SimpleComponent extends Component {
  render() {
    const {
            aStateProp,
            classes,
            someAction,
          } = this.props

    return <div
      className={classes.aClassName}
      onClick  ={someAction}
    >
      {aStateProp}
    </div>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  style,
})(SimpleComponent)
```

### Debugging

A custom function `debugValues` is available globally for quick investigating.
It's provides data in the console in a way that makes it easy to spot.

For example:

```javascript
let someVar    = 'hello'
let anotherVar = {text: 'world'}

debugValues('Example.example(), {
  someVar,
  anotherVar,
})
```

produces in the console:

```
╒══ Example.example()
├ someVar 'hello'
├ anotherVar {text: 'world'}
╘══ END Example.example()
```
