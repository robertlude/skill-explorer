// Internal Dependencies

import {
  CHXBOT_BUCKET__STANDARD,
  CHXBOT_BUCKET__OLD,
} from '@/chxbotBuckets'

import {
  CHXBOT_VERSION__LATEST,
  CHXBOT_VERSION__NIGHTLY,
  CHXBOT_VERSION__SNAPSHOT,
  CHXBOT_VERSION__SPECIFIC,
} from '@/chxbotVersions'

// Module

const alternatePath = /^https:\/\/s3\.amazonaws\.com\/olive-release-artifacts\/chxbot\/(.*?)\/chxbot.zip$/
    , oldBucketPath = /^https:\/\/s3\.amazonaws\.com\/olive-assets-secure\/skill_resources_public\/chxbot\/(.*?)\/chxbot.zip$/
    , specificRegex = /^\d+\.\d+\.\d+$/
    , standardPath  = /^https:\/\/olive-release-artifacts\.s3\.amazonaws\.com\/chxbot\/(.*?)\/chxbot.zip$/

export default download => {
  let bucket
    , match
    , version

  if (match = oldBucketPath.exec(download)) {
    bucket  = CHXBOT_BUCKET__OLD
    version = match[1]
  } else if (  (match = standardPath.exec(download))
            || (match = alternatePath.exec(download))
            ) {
    bucket  = CHXBOT_BUCKET__STANDARD
    version = match[1]
  } else {
    return {matched: false}
  }

  if (version == 'LATEST') return {
    matched: true,
    result: {
      version: CHXBOT_VERSION__LATEST,
      bucket,
    },
  }

  if (version == 'NIGHTLY') return {
     matched: true,
     result: {
       version: CHXBOT_VERSION__NIGHTLY,
       bucket,
     },
   }

  if (version.startsWith('SNAPSHOT/')) return {
    matched: true,
    result: {
      ref:     version.slice(9),
      version: CHXBOT_VERSION__SNAPSHOT,
      bucket,
    },
  }

  if (specificRegex.test(version)) return {
    matched: true,
    result: {
      number:  version,
      version: CHXBOT_VERSION__SPECIFIC,
      bucket
    },
  }

  return {matched: false}
}
