// Module

const automationFlowsBranchRegex = /^https:\/\/s3\.amazonaws.com\/olive-assets-secure\/olive-automation-flows\/SNAPSHOT\/(.*?)\/(.*\.(.*))$/
    , automationFlowsMasterRegex = /^https:\/\/s3\.amazonaws.com\/olive-assets-secure\/olive-automation-flows\/(?!SNAPSHOT)(.*\.(.*))$/

export default download => {
  let match

  if (match = automationFlowsMasterRegex.exec(download)) {
    return {
      matched: true,
      result: {
        branch: 'master',
        path:   match[1],
        type:   match[2],
      },
    }
  }

  if (match = automationFlowsBranchRegex.exec(download)) {
    return {
      matched: true,
      result: {
        branch: match[1],
        path:   match[2],
        type:   match[3],
      },
    }
  }

  return {matched: false}
}
