// External Dependencies

import uuid from 'uuid/v4'

// Internal Dependencies

import extractAutomationFlowsDownload from './transformSkill/extractAutomationFlowsDownload'
import extractChxBot                  from './transformSkill/extractChxBot'

import { CHXBOT_VERSION__NONE } from '@/chxbotVersions'

// Module

export default data => {
  data.automationFlowsDownloads = []
  data.revision                 = uuid()

  data.otherDownloads = data.downloads.filter(download => {
    let match

    if ((match = extractAutomationFlowsDownload(download)).matched) {
      data.automationFlowsDownloads.push(match.result)
      return false
    }

    if ((match = extractChxBot(download)).matched) {
      data.chxBot = match.result
      return false
    }

    return true
  })

  if (!data.chxBot) data.chxBot = {version: CHXBOT_VERSION__NONE}

  delete data.downloads

  return data
}
