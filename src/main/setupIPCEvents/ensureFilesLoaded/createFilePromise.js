// External Dependencies

import fs from 'fs'

// Internal Dependencies

import transformSkill from './createFilePromise/transformSkill'

import isSkillFile from '@/main/isSkillFile'

import findEntry from '@/findEntry'

import {
  FILE_FORMAT__JSON,
  FILE_FORMAT__SKILL,
  FILE_FORMAT__UNKNOWN,
} from '@/fileFormats'

// Module

export default appData => path => {
  const { entries } = appData.store.getState().skills

      , entry = findEntry(entries, path)

  if (entry && !entry.loaded) {
    return new Promise((resolve, reject) => {
      fs.readFile(
        entry.path,
        {encoding: 'utf8'},
        (error, contents) => {
          if (error) {
            console.error('ensureFilesLoaded error:', error)
            resolve()
          }

          let format = FILE_FORMAT__UNKNOWN

          if (path.endsWith('.json')) {
            contents = JSON.parse(contents)

            if (isSkillFile(contents)) {
              contents = transformSkill(contents)
              format   = FILE_FORMAT__SKILL
            } else {
              format = FILE_FORMAT__JSON
            }
          }

          resolve({contents, format, path})
        }
      )
    })
  }
}
