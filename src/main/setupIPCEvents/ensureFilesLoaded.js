// Internal Dependencies

import createFilePromise from './ensureFilesLoaded/createFilePromise'

import { setFileContents } from '@/actions'

// Module

export default appData => (event, paths) => {
  const { entries } = appData.store.getState().skills

      , promiseGenerator = createFilePromise(appData)

      , filePromises = paths
                         .map(promiseGenerator)
                         .filter(promise => promise)

  Promise
    .all(filePromises)
    .then(newEntries => {
      const cleanEntries = newEntries.filter(entry => entry)

      appData
        .store
        .dispatch(setFileContents(cleanEntries))
    })
}
