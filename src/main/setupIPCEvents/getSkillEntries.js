// Internal Depenencies

import getSkills from './getSkillEntries/getSkills'

import { setSkillEntries } from '@/actions'

// Module

export default appData => () => {
  const { oliveSkillsPath } = appData.store.getState().settings

  getSkills(oliveSkillsPath)
    .then(result => appData.store.dispatch(setSkillEntries(result)))
    .catch(error => {
      appData.store.dispatch(setSkillEntries([]))
      console.error('getSkillEntries error:', error)
    })
}
