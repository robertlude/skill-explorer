// Internal Dependencies

import { IPC_EVENT_TYPE__SET_INITIAL_STATE } from '@/ipcEventTypes'

// Module

export default appData => () =>
  appData
    .appWindow
    .webContents
    .send(
      IPC_EVENT_TYPE__SET_INITIAL_STATE,
      appData.store.getState(),
    )
