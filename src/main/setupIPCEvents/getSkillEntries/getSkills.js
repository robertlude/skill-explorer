// External Dependencies

import path from 'path'

// Internal Dependencies

import skillGetter from './getSkills/skillGetter'

// Module

export default async oliveSkillsPath => skillGetter(path.resolve(
  oliveSkillsPath,
  'skill_definitions',
  'main',
))
