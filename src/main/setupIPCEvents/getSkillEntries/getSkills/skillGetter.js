// External Dependencies

import fs from 'fs'

// Internal Dependencies

import skillFilter from './skillGetter/skillFilter'
import skillSorter from './skillGetter/skillSorter'

import skillMapper from './skillMapper'

// Module

export default oliveSkillsPath => {
  const pathSkillMapper = skillMapper(oliveSkillsPath)

  return fs
           .readdirSync(oliveSkillsPath, {withFileTypes: true})
           .map(pathSkillMapper)
           .filter(skillFilter)
           .sort(skillSorter)
}
