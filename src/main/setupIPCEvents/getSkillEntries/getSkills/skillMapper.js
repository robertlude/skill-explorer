// External Dependencies

import path from 'path'

// Internal Dependencies

import skillGetter from './skillGetter'

import { FILE_FORMAT__UNKNOWN } from '@/fileFormats'

import {
  ENTRY_TYPE__DIRECTORY,
  ENTRY_TYPE__FILE,
  ENTRY_TYPE__IGNORE,
} from '@/entryTypes'

// Module

export default basePath => entry => {
  if (entry.name[0] == '.') return { type: ENTRY_TYPE__IGNORE }

  const entryPath = path.resolve(basePath, entry.name)

  if (entry.isFile()) return {
    contents: '',
    format:   FILE_FORMAT__UNKNOWN,
    loaded:   false,
    name:     entry.name,
    path:     entryPath,
    type:     ENTRY_TYPE__FILE,
  }

  return {
    entries:  skillGetter(entryPath),
    expanded: false,
    name:     entry.name,
    path:     entryPath,
    type:     ENTRY_TYPE__DIRECTORY,
  }
}
