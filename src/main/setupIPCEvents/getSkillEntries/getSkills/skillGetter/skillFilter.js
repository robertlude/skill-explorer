// Internal Dependenices

import { ENTRY_TYPE__IGNORE } from '@/entryTypes'

// Module

export default entry => entry.type != ENTRY_TYPE__IGNORE
