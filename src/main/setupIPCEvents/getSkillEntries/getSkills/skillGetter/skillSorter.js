// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

export default (a, b) =>
  a.type == b.type
  ? a.name.localeCompare(b.name)
  : ( a.type == ENTRY_TYPE__DIRECTORY
    ? -1
    : 1
    )
