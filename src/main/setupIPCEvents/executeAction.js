// Module

export default appData => (event, action) => {
  action.fromRenderer = true

  appData.store.dispatch(action)
}
