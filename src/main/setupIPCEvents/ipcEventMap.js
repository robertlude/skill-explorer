// Internal Dependencies

import ensureAllFilesLoaded from './ensureAllFilesLoaded'
import ensureFilesLoaded    from './ensureFilesLoaded'
import executeAction        from './executeAction'
import getInitialState      from './getInitialState'
import getSkillEntries      from './getSkillEntries'

import {
  IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED,
  IPC_EVENT_TYPE__ENSURE_FILES_LOADED,
  IPC_EVENT_TYPE__EXECUTE_ACTION,
  IPC_EVENT_TYPE__GET_INITIAL_STATE,
  IPC_EVENT_TYPE__GET_SKILL_ENTRIES,
} from '@/ipcEventTypes'

// Module

const ipcEventMap = {
  [IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED]: ensureAllFilesLoaded,
  [IPC_EVENT_TYPE__ENSURE_FILES_LOADED]:     ensureFilesLoaded,
  [IPC_EVENT_TYPE__EXECUTE_ACTION]:          executeAction,
  [IPC_EVENT_TYPE__GET_INITIAL_STATE]:       getInitialState,
  [IPC_EVENT_TYPE__GET_SKILL_ENTRIES]:       getSkillEntries,
}

export default ipcEventMap
