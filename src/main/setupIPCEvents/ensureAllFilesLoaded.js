// Internal Dependencies

import ensureFilesLoaded from './ensureFilesLoaded'

import getAllSkillEntryFiles from '@/getAllSkillEntryFiles'

// Module

export default appData => event => {
  const paths = getAllSkillEntryFiles(appData.store.getState().skills.entries)
                  .map(entry => entry.path)

  ensureFilesLoaded(appData)(event, paths)
}
