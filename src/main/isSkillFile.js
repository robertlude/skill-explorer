// External Dependencies

import _ from 'lodash'

// Module

const skillFileKeys = [
  'ami',
  'command',
  'commandEnvPath',
  'description',
  'downloads',
  'env',
  'name',
  'outputGlobs',
  'type',
]

export default data => _.isEqual(
  _.intersection(
    Object.keys(data),
    skillFileKeys,
  ).sort(),
  skillFileKeys,
)
