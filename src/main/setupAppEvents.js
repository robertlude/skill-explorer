// External Dependencies

import { app } from 'electron'

// Internal Dependencies

import appEventMap from './setupAppEvents/appEventMap'

// Module

export default appData =>
  Object
    .entries(appEventMap)
    .forEach(([eventType, eventHandler]) =>
      app.on(eventType, eventHandler(appData))
    )
