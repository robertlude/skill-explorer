// Internal Dependencies

import { IPC_EVENT_TYPE__EXECUTE_ACTION } from '@/ipcEventTypes'

// Module

export default appData => store => next => action => {
  if (!action.fromRenderer) appData
                              .appWindow
                              .webContents
                              .send(
                                IPC_EVENT_TYPE__EXECUTE_ACTION,
                                action
                              )

  return next(action)
}
