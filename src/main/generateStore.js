// External Dependencies

import {
  applyMiddleware,
  createStore,
} from 'redux'

// Internal Dependencies

import sendActionToRenderer from './generateStore/sendActionToRenderer'

import actionLogger from '@/actionLogger'
import reducers     from '@/reducers'

// Module

export default appData => {
  appData.store = createStore(
    reducers,
    applyMiddleware(
      actionLogger,
      sendActionToRenderer(appData),
    ),
  )
}
