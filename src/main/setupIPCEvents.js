// External Dependencies

import { ipcMain } from 'electron'

// Internal Dependencies

import ipcEventMap from './setupIPCEvents/ipcEventMap'

// Module

export default appData =>
  Object
    .entries(ipcEventMap)
    .forEach(([eventType, eventHandler]) =>
      ipcMain.on(eventType, eventHandler(appData))
    )
