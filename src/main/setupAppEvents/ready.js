// Internal Dependencies

import createWindow from './createWindow'

// Module

export default appData => () => {
  appData.appWindow = createWindow()
}
