// Internal Dependencies

import createWindow from './createWindow'

// Module

export default appData => () => {
  if (!appData.appWindow) appData.appWindow = createWindow()
}
