// External Dependencies

import { BrowserWindow } from 'electron'

// Module

export default () => {
  const appWindow = new BrowserWindow({
    width:  1200,
    height: 800,

    backgroundColor: '#000',

    webPreferences: {
      nodeIntegration: true,
    },
  })

  appWindow.loadFile('index.html')
  appWindow.setFullScreen(true)

  return appWindow
}
