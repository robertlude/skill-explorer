// Internal Dependencies

import activate        from './activate'
import ready           from './ready'
import windowAllClosed from './windowAllClosed'

// Module

const appEventMap = {
  'activate':          activate,
  'ready':             ready,
  'window-all-closed': windowAllClosed,
}

export default appEventMap
