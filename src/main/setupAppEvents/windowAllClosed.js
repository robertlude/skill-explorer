// External Dependencies

import { app } from 'electron'

// Module

export default appData => () => {
  if (process.platform == 'darwin') app.quit()
}
