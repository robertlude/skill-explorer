// Module

const ENTRY_TYPE__DIRECTORY = 'entryType:directory'
    , ENTRY_TYPE__FILE      = 'entryType:file'
    , ENTRY_TYPE__IGNORE    = 'entryType:ignore'

export {
  ENTRY_TYPE__DIRECTORY,
  ENTRY_TYPE__FILE,
  ENTRY_TYPE__IGNORE,
}
