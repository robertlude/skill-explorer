// Module

module.exports = (caption, values) => {
  console.log(`\u2552\u2550\u2550 ${caption}`)

  for (let key in values) {
    console.log(`\u251C ${key}`, values[key])
  }

  console.log(`\u2558\u2550\u2550 END ${caption}`)
}
