// Module

const MODAL_TYPE__CONSOLE     = 'modalType:console'
    , MODAL_TYPE__SETTINGS    = 'modalType:settings'
    , MODAL_TYPE__UI_DISABLER = 'modalType:uiDisabler'

export {
  MODAL_TYPE__CONSOLE,
  MODAL_TYPE__SETTINGS,
  MODAL_TYPE__UI_DISABLER,
}
