// External Dependencies

import React, {
  Component,
} from 'react'

import {
  Collapse,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core'

// Internal Dependencies

import Error        from './SkillList/Error'
import Filter       from './SkillList/Filter'
import createEntry  from './SkillList/createEntry'
import createFilter from './SkillList/createFilter'
import stateProps   from './SkillList/stateProps'

import wrap from './wrap'

// Module

class SkillList extends Component {
  render() {
    const {
            entries,
            filter,
            nesting,
          } = this.props

    if (entries.length == 0)
      return <Error />

    return <List
      component     ={nesting ? 'div' : undefined}
      disablePadding={!!nesting}
      dense
    >
      {createFilter(nesting)}
      {entries.map(entry => createEntry(entry, filter, nesting))}
    </List>
  }
}

export default wrap({stateProps})(SkillList)
