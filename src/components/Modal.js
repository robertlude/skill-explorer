// External Dependencies

import {
  Grid,
  Paper,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Header from './Modal/Header'
import Footer from './Modal/Footer'
import styles from './Modal/styles'

import BackgroundBlur from 'c/BackgroundBlur'
import createRefs     from 'c/createRefs'
import wrap           from 'c/wrap'

// Component

class Modal extends Component {
  constructor(props) {
    super(props)

    createRefs(this, 'background')
  }

  render() {
    const { background } = this
        , {
            cancelIcon,
            cancelText,
            children,
            classes,
            confirmIcon,
            confirmText,
            modalId,
            onCancel,
            onConfirm,
            paperClass,
            titleIcon,
            titleText,
          } = this.props

    return <BackgroundBlur ref={background}>
      <Paper className={paperClass}>
        <Grid
          className={classes.container}
          container
        >
          <Grid
            className={classes.header}
            xs       ={12}
            item
          >
            <Header
              icon ={titleIcon}
              title={titleText}
            />
          </Grid>
          <Grid
            className={classes.content}
            xs       ={12}
            item
          >
            {children}
          </Grid>
          <Grid
            className={classes.footer}
            xs       ={12}
            item
          >
            <Footer
              cancelIcon ={cancelIcon}
              cancelText ={cancelText}
              confirmIcon={confirmIcon}
              confirmText={confirmText}
              modalId    ={modalId}
              onCancel   ={onCancel}
              onConfirm  ={onConfirm}
            />
          </Grid>
        </Grid>
      </Paper>
    </BackgroundBlur>
  }

  close() {
    this.background.current.close(this.props.modalId)
  }
}

export default wrap({styles})(Modal)
