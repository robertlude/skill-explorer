// Internal Dependencies

import {
  redo,
  showModal,
  undo,
} from '@/actions'

// Modules

export default {
  redo,
  showModal,
  undo,
}
