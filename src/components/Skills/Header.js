// External Dependencies

import CodeIcon     from '@material-ui/icons/Code'
import RedoIcon     from '@material-ui/icons/Redo'
import SettingsIcon from '@material-ui/icons/Settings'
import UndoIcon     from '@material-ui/icons/Undo'

import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './Header/dispatchProps'
import stateProps    from './Header/stateProps'
import styles        from './Header/styles'

import wrap from 'c/wrap'

import findEntry from '@/findEntry'

import {
  MODAL_TYPE__CONSOLE,
  MODAL_TYPE__SETTINGS,
} from '@/modalTypes'

// Module

class Header extends Component {
  render() {
    const {
            classes,
            redo,
            showModal,
            skills,
            undo,
          } = this.props

        , entry = findEntry(skills.entries, skills.selectedFile)

    let canRedo
      , canUndo

    if (entry) {
      canRedo = !!entry.future
      canUndo = !!entry.past
    } else {
      canRedo = false
      canUndo = false
    }

    return <AppBar position='fixed' className={classes.appBar}>
      <Toolbar variant='dense'>
        <Typography variant='h6' style={{flex: 1}}>Skill Explorer</Typography>
        <IconButton
          disabled={!canUndo}
          onClick ={undo}
        >
          <UndoIcon />
        </IconButton>
        <IconButton
          disabled={!canRedo}
          onClick ={redo}
        >
          <RedoIcon />
        </IconButton>
        <IconButton onClick={() => showModal(MODAL_TYPE__CONSOLE)}>
          <CodeIcon />
        </IconButton>
        <IconButton onClick={() => showModal(MODAL_TYPE__SETTINGS)}>
          <SettingsIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  styles,
})(Header)
