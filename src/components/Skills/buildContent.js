// External Dependencies

import React              from 'react'
import { LinearProgress } from '@material-ui/core'

// Internal Dependencies

import SkillEditor from 'c/SkillEditor'

import {
  FILE_FORMAT__JSON,
  FILE_FORMAT__SKILL,
  FILE_FORMAT__UNKNOWN,
} from '@/fileFormats'

// Module

export default selectedEntry => {
  if (!selectedEntry)
    return <div>No file selected.</div>

  if (!selectedEntry.loaded)
    return <LinearProgress />

  switch (selectedEntry.format) {
    case FILE_FORMAT__JSON:
      return <div>Hello, JSON</div>
    case FILE_FORMAT__SKILL:
      return <SkillEditor
        entry={selectedEntry}
        key  ={selectedEntry.revision}
      />
    case FILE_FORMAT__UNKNOWN:
      return <div>Hello, unknown</div>
    default:
      return <div>Unsupported file format</div>
  }
}
