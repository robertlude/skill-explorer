// External Dependencies

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './BackgroundBlur/dispatchProps'
import styles        from './BackgroundBlur/styles'

import createRefs from 'c/createRefs'
import wrap       from 'c/wrap'

// Module

class BackgroundBlur extends Component {
  constructor(props) {
    super(props)

    createRefs(this, 'background')
  }

  render() {
    const { background } = this
        , {
            children,
            classes,
          } = this.props

    return <div
      className={classes.backgroundBlur}
      ref      ={background}
    >
      {children}
    </div>
  }

  componentDidMount() {
    this
      .background
      .current
      .classList
      .add('blurIn')
  }

  close(modalId) {
    const { classList }         = this.background.current
        , { hideModalComplete } = this.props

    classList.remove('blurIn')
    classList.add('blurOut')

    setTimeout(() => hideModalComplete(modalId), 200)
  }
}

export default wrap({
  forwardRef: true,
  dispatchProps,
  styles,
}
)(BackgroundBlur)
