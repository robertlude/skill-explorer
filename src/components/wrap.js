// Internal Dependencies

import wrapConnect from './wrap/wrapConnect'
import wrapStyles  from './wrap/wrapStyles'

// Module

export default (data = {}) => component =>
  wrapConnect(data)(
    wrapStyles(data)(
      component
    )
  )
