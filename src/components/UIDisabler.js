// External Dependencies

import Typography from '@material-ui/core/Typography'

import React, {
  Component,
  createRef,
} from 'react'

// Internal Dependencies

import dispatchProps from './UIDisabler/dispatchProps'
import styles        from './UIDisabler/styles'

import BackgroundBlur from 'c/BackgroundBlur'
import wrap           from 'c/wrap'

// Module

class UIDisabler extends Component {
  constructor(props) {
    super(props)

    this.background = createRef()
  }

  render() {
    const { background } = this
        , {
            classes,
            message,
          } = this.props

    return <BackgroundBlur ref={background}>
      <div className={classes.uiDisabler}>
        <Typography>{message}</Typography>
      </div>
    </BackgroundBlur>
  }

  componentDidUpdate() {
    const background = this.background.current
        , {
            animateClose,
            enableUIComplete,
          } = this.props

    if (animateClose) background.close(enableUIComplete)
  }
}

export default wrap({
  forwardRef: true,
  dispatchProps,
  styles,
})(UIDisabler)
