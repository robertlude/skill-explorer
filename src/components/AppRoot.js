// External Dependencies

import ReactDOM          from 'react-dom'
import { CssBaseline }   from '@material-ui/core'
import { ThemeProvider } from '@material-ui/core/styles'
import { connect }       from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import generateModals     from './AppRoot/generateModals'
import generateUIDisabler from './AppRoot/generateUIDisabler'
import stateProps         from './AppRoot/stateProps'

import Background from 'c/Background'
import Skills     from 'c/Skills'
import wrap       from 'c/wrap'

import theme from '@/theme'

// Module

class AppRoot extends Component {
  render() {
    const {
            disabler,
            modals,
          } = this.props

    return <ThemeProvider theme={theme}>
      <CssBaseline />
      <Background />
      <Skills />
      {generateModals(modals)}
      {generateUIDisabler(disabler)}
    </ThemeProvider>
  }
}

export default wrap({stateProps})(AppRoot)
