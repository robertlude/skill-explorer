// External Dependencies

import SearchIcon from '@material-ui/icons/Search'

import {
  Divider,
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './Filter/dispatchProps'
import stateProps    from './Filter/stateProps'
import styles        from './Filter/styles'

import wrap from 'c/wrap'

// Module

class Filter extends Component {
  render() {
    const {
      classes,
      filter,
      setFilter,
    } = this.props

    return <div>
      <div className={classes.container}>
        <FormControl fullWidth>
          <InputLabel htmlFor='skill-list-filter'>Filter</InputLabel>
          <Input
            id   ='skill-list-filter'
            value={filter}
            onChange={event => setFilter(event.target.value)}
            endAdornment={
              <InputAdornment position='end'>
                <SearchIcon />
              </InputAdornment>
            }
          />
        </FormControl>
      </div>
      <Divider />
    </div>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  styles,
})(Filter)
