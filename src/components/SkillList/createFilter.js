// External Dependencies

import React from 'react'

// Internal Dependencies

import Filter from './Filter'

// Module

export default nesting =>
  nesting
  ? null
  : <Filter />
