// External Dependencies

import React from 'react'

// Internal Dependencies

import isVisible from './createEntry/isVisible'

import Entry from './Entry'

// Module

export default (entry, filter, nesting) =>
  isVisible(entry, filter)
  ? <Entry
      entry  ={entry}
      key    ={entry.path}
      nesting={nesting}
    />
  : null
