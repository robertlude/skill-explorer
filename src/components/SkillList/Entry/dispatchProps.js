// Internal Dependencies

import {
  selectFile,
  toggleDirectory,
} from '@/actions'

// Module

export default {
  selectFile,
  toggleDirectory,
}
