// Module

export default theme => ({
  nesting1: {
    paddingLeft: theme.spacing(4),
  },
  nesting2: {
    paddingLeft: theme.spacing(6),
  },
  nesting3: {
    paddingLeft: theme.spacing(8),
  },
  nesting4: {
    paddingLeft: theme.spacing(10),
  },
  nesting5: {
    paddingLeft: theme.spacing(12),
  },
  nesting6: {
    paddingLeft: theme.spacing(14),
  },
})
