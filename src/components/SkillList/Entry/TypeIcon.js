// External Dependencies

import DescriptionIcon from '@material-ui/icons/Description'
import FolderIcon      from '@material-ui/icons/Folder'

import { ListItemIcon } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

class TypeIcon extends Component {
  render() {
    const { type } = this.props

    return <ListItemIcon>
      { type == ENTRY_TYPE__DIRECTORY
      ? <FolderIcon />
      : <DescriptionIcon />
      }
    </ListItemIcon>
  }
}

export default wrap()(TypeIcon)
