// Internal Dependencies

import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import React from 'react'

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

export default entry =>
  entry.type == ENTRY_TYPE__DIRECTORY
  ? ( entry.expanded
    ? <ExpandLessIcon />
    : <ExpandMoreIcon />
    )
  : null
