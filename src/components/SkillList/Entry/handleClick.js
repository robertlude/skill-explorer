// External Dependencies

import { ipcRenderer } from 'electron'

// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

import { IPC_EVENT_TYPE__ENSURE_FILES_LOADED } from '@/ipcEventTypes'

// Module

export default (entry, {path, type}) =>
  type == ENTRY_TYPE__DIRECTORY
  ? () => entry.props.toggleDirectory(path)
  : () => {
      ipcRenderer.send(IPC_EVENT_TYPE__ENSURE_FILES_LOADED, [path])
      entry.props.selectFile(path)
    }
