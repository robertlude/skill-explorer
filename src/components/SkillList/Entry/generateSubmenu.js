// External Dependencies

import React        from 'react'
import { Collapse } from '@material-ui/core'

// Internal Dependencies

import SkillList from 'c/SkillList'

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

export default (entry, nesting) =>
  entry.type != ENTRY_TYPE__DIRECTORY
  ? null
  : <Collapse
      in     ={entry.expanded}
      timeout='auto'
      unmountOnExit
    >
      <SkillList
        entries={entry.entries}
        nesting ={(nesting || 0) + 1}
      />
    </Collapse>

