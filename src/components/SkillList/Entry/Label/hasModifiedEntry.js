// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

const hasModifiedEntry = entries => {
  for (let entry of entries) {
    if (  entry.type == ENTRY_TYPE__DIRECTORY
       && hasModifiedEntry(entry.entries)
       ) return true

    if (entry.modified) return true
  }

  return false
}

export default hasModifiedEntry
