// External Dependencies

import {
  ListItemText,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import hasModifiedEntry from './Label/hasModifiedEntry'
import styles           from './Label/styles'

import wrap from 'c/wrap'

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

class Label extends Component {
  render() {
    const {
            classes,
            entry,
          } = this.props

        , modified = entry.modified
                   ? true
                   : ( entry.type == ENTRY_TYPE__DIRECTORY
                     ? hasModifiedEntry(entry.entries)
                     : false
                     )

        , className = modified
                    ? classes.modified
                    : ''
        , text      = modified
                    ? `${entry.name}*`
                    : entry.name

    return <ListItemText
      className={className}
      primary  ={text}
    />
  }
}

export default wrap({styles})(Label)
