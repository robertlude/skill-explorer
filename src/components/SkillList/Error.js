// External Dependencies

import SettingsIcon from '@material-ui/icons/Settings'

import React, {
  Component,
} from 'react'

import {
  Button,
  Typography,
} from '@material-ui/core'

// Internal Dependencies

import dispatchProps from './Error/dispatchProps'
import styles        from './Error/styles'

import wrap from 'c/wrap'

import { MODAL_TYPE__SETTINGS } from '@/modalTypes'

// Module

class Error extends Component {
  render() {
    const {
      classes,
      showModal,
    } = this.props

    return <div className={classes.error}>
      <Typography>
        An error occurred retrieving the skills list.
        Please check your settings to ensure a
        valid <code>olive-skills</code> path has been
        set.
      </Typography>
      <br />
      <Button
        color    ='primary'
        onClick  ={() => showModal(MODAL_TYPE__SETTINGS)}
        startIcon={<SettingsIcon />}
        variant  ='contained'
      >
        Settings
      </Button>
    </div>
  }
}

export default wrap({
  dispatchProps,
  styles,
})(Error)
