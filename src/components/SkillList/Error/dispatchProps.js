// Internal Dependencies

import { showModal } from '@/actions'

// Module

export default { showModal }
