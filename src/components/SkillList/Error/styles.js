// Module

export default theme => ({
  error: {
    padding:   theme.spacing(3),
    textAlign: 'center',
  },
})
