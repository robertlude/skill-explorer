// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Module

const isVisible = (entry, filter) => {
  if (entry.path.includes(filter)) return true

  if (entry.type == ENTRY_TYPE__DIRECTORY)
    return entry.entries.some(entry => isVisible(entry, filter))

  return false
}

export default isVisible
