// Internal Dependencies

import { setFilter } from '@/actions'

// Module

export default {
  setFilter,
}
