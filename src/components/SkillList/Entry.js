// External Dependencies

import {
  Collapse,
  ListItem,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Label              from './Entry/Label'
import TypeIcon           from './Entry/TypeIcon'
import dispatchProps      from './Entry/dispatchProps'
import generateExpandIcon from './Entry/generateExpandIcon'
import generateSubmenu    from './Entry/generateSubmenu'
import handleClick        from './Entry/handleClick'
import stateProps         from './Entry/stateProps'
import styles             from './Entry/styles'

import wrap from 'c/wrap'

// Module

class Entry extends Component {
  render() {
    const {
            ListComponent,
            classes,
            entry,
            nesting,
            selectedFile,
          } = this.props

        , {
            expanded,
            modified,
            path,
            type,
          } = entry

    return <div>
      <ListItem
        button
        className={nesting ? classes[`nesting${nesting}`] : ''}
        onClick  ={handleClick(this, entry)}
        selected ={path == selectedFile}
      >
        <TypeIcon type={type} />
        <Label entry={entry} />
        {generateExpandIcon(entry)}
      </ListItem>
      {generateSubmenu(entry, nesting)}
    </div>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  styles,
})(Entry)
