// Internal Dependencies

import { enableUIComplete } from '@/actions'

// Module

export default { enableUIComplete }
