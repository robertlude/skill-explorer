// Module

export default theme => ({
  backgroundBlur: {
    position:   'fixed',
    left:       0,
    right:      0,
    top:        0,
    bottom:     0,

    background:     'rgba(0, 0, 0, 0.667)',
    textAlign:      'center',
    zIndex:         1000000,
  }
})
