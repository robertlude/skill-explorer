// Internal Dependencies

import { hideModalComplete } from '@/actions'

// Module

export default { hideModalComplete }
