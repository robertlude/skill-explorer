// External Dependencies

import ExpandMoreIcon     from '@material-ui/icons/ExpandMore'
import ExpandLessIcon     from '@material-ui/icons/ExpandLess'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import path               from 'path'
import { URL }            from 'url'

import {
  Box,
  Collapse,
  IconButton,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class DownloadListItem extends Component {
  constructor(props) {
    super(props)

    this.state = {expanded: false}
  }

  render() {
    const { expanded } = this.state
        , {
            item,
            onDelete,
          } = this.props

        , url = new URL(item)

        , fileName = path.basename(url.pathname)

    return <div>
      <ListItem>
        <ListItemText>
          <Box fontFamily='Monospace'>
            {fileName}
          </Box>
        </ListItemText>
        <ListItemSecondaryAction>
          <IconButton
            edge   ='end'
            onClick={event => onDelete(item)}
          >
            <DeleteOutlinedIcon />
          </IconButton>
          <IconButton
            edge   ='end'
            onClick={() => this.setState({expanded: !expanded})}
          >
            { expanded
            ? <ExpandLessIcon />
            : <ExpandMoreIcon />
            }
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Collapse in={expanded}>
        {item}
      </Collapse>
    </div>
  }
}

export default wrap()(DownloadListItem)
