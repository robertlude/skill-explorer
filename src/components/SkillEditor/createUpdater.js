// Internal Dependencies

import scheduleUpdate from './scheduleUpdate'

// Module

export default (editor, path, attribute, transform) => {
  if (!transform) return event => scheduleUpdate(
                           editor,
                           path,
                           attribute,
                           event.target.value,
                         )

  return event => scheduleUpdate(
                    editor,
                    path,
                    attribute,
                    transform(event.target.value),
                  )
}
