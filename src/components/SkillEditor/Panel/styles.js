// Module

export default theme => ({
  container: {
    flexGrow: 1,
    padding:  theme.spacing(3, 2),
  },
})
