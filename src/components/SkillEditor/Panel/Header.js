// External Dependencies

import { Typography } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class PanelHeader extends Component {
  render() {
    const { children } = this.props

    return <Typography
      component='h3'
      variant  ='h5'
    >
      {children}
    </Typography>
  }
}

export default wrap()(PanelHeader)
