// External Dependencies

import { Paper } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Header from './Panel/Header'
import styles from './Panel/styles'

import wrap from 'c/wrap'

// Module

class Panel extends Component {
  render() {
    const {
            classes,
            title,
            children,
          } = this.props

    return <Paper className={classes.container}>
      <Header>{title}</Header>
      {children}
    </Paper>
  }
}

export default wrap({styles})(Panel)
