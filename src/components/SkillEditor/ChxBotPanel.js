// External Dependencies

import {
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import createBucketOption     from './ChxBotPanel/createBucketOption'
import createVersionInterface from './ChxBotPanel/createVersionInterface'
import styles                 from './ChxBotPanel/styles'

import Panel from './Panel'

import wrap from 'c/wrap'

import {
  CHXBOT_BUCKET__STANDARD,
  CHXBOT_BUCKET__OLD,
} from '@/chxbotBuckets'

import {
  CHXBOT_VERSION__LATEST,
  CHXBOT_VERSION__NIGHTLY,
  CHXBOT_VERSION__NONE,
  CHXBOT_VERSION__SNAPSHOT,
  CHXBOT_VERSION__SPECIFIC,
} from '@/chxbotVersions'

// Module

class ChxBotPanel extends Component {
  render() {
    const { chxBot }  = this.props.entry.contents
        , { classes } = this.props

    return <Panel title='ChxBot'>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>Version</InputLabel>
            <Select value={chxBot.version}>
              <MenuItem value={CHXBOT_VERSION__NONE}>None</MenuItem>
              <MenuItem value={CHXBOT_VERSION__SPECIFIC}>Specific</MenuItem>
              <MenuItem value={CHXBOT_VERSION__SNAPSHOT}>Snapshot</MenuItem>
              <MenuItem value={CHXBOT_VERSION__LATEST}>Latest</MenuItem>
              <MenuItem value={CHXBOT_VERSION__NIGHTLY}>Nightly</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>Bucket</InputLabel>
            <Select value={chxBot.bucket}>
              {createBucketOption({
                label: 'Standard',
                path:  'https://olive-release-artifacts.s3.amazonaws.com/chxbot/*/chxbot.zip',
                value: CHXBOT_BUCKET__STANDARD,
                classes,
              })}
              {createBucketOption({
                label: 'Old',
                path:  'https://s3.amazonaws.com/olive-asset-secure/skill_resources_public/chxbot/*/chxbot.zip',
                value: CHXBOT_BUCKET__OLD,
                classes,
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          {createVersionInterface(chxBot)}
        </Grid>
      </Grid>
    </Panel>
  }
}

export default wrap({styles})(ChxBotPanel)
