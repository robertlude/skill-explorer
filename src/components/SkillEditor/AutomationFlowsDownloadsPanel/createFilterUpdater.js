// Module

export default component => (type, branch, path) => {
  component.setState({
    filters: {
      branch,
      path,
      type,
    },
  })
}
