// Module

export default (downloads, filters) => {
  return downloads.filter(download => {
    if (  filters.type != '*'
       && download.type != filters.type
       ) return false

    if (  filters.branch != '*'
       && download.branch != filters.branch
       ) return false

    if (  filters.path != ''
       && !download.path.includes(filters.path)
       ) return false

    return true
  })
}
