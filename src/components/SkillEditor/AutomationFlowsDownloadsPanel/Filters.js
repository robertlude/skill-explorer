// External Dependencies

import SearchIcon from '@material-ui/icons/Search'

import {
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class Filters extends Component {
  render() {
    const {
            branches,
            filters,
            onChange,
            types,
          } = this.props

        , {
            branch,
            path,
            type,
          } = filters

    return <TableHead>
      <TableRow>
        <TableCell>
          <FormControl>
            <InputLabel id='automation-flows-downloads-type'>
              Type
            </InputLabel>
            <Select
              labelId ='automation-flows-downloads-type'
              onChange={event => onChange(event.target.value, branch, path)}
              value   ={filters.type}
            >
              <MenuItem value='*'>All</MenuItem>
              {types.map(type =>
                <MenuItem
                  key  ={type}
                  value={type}
                >
                  {type}
                </MenuItem>
              )}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell>
          <FormControl>
            <InputLabel id='automation-flows-downloads-branch'>
              Branch
            </InputLabel>
            <Select
              labelId ='automation-flows-downloads-branch'
              onChange={event => onChange(type, event.target.value, path)}
              value   ={filters.branch}
            >
              <MenuItem value='*'>All</MenuItem>
              {branches.map(branch =>
                <MenuItem
                  key  ={branch}
                  value={branch}
                >
                  {branch}
                </MenuItem>
              )}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell>
          <FormControl fullWidth>
            <InputLabel htmlFor='automation-flows-downloads-path-filter'>
              Path
            </InputLabel>
            <Input
              id      ='automation-flows-downloads-path-filter'
              onChange={event => onChange(type, branch, event.target.value)}
              value   ={filters.path}
              endAdornment={
                <InputAdornment position='end'>
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </FormControl>
        </TableCell>
      </TableRow>
    </TableHead>
  }
}

export default wrap()(Filters)
