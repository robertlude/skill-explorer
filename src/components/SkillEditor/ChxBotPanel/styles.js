// Module

export default theme => ({
  glob: {
    color:      theme.palette.primary.main,
    fontWeight: 'bold',
  },
  label: {
    fontWeight: 'bold',
  }
})
