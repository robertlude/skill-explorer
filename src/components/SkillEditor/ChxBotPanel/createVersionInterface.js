// External Dependencies

import {
  FormControl,
  Input,
  InputLabel,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import {
  CHXBOT_VERSION__LATEST,
  CHXBOT_VERSION__NIGHTLY,
  CHXBOT_VERSION__NONE,
  CHXBOT_VERSION__SNAPSHOT,
  CHXBOT_VERSION__SPECIFIC,
} from '@/chxbotVersions'

// Module

export default chxBot => {
  switch (chxBot.version) {
    case CHXBOT_VERSION__LATEST:
    case CHXBOT_VERSION__NIGHTLY:
    case CHXBOT_VERSION__NONE:
      return null
    case CHXBOT_VERSION__SNAPSHOT:
      return <FormControl fullWidth>
        <InputLabel htmlFor='chxbot-snapshot-ref'>Branch/Commit</InputLabel>
        <Input id='chxbot-snapshot-ref' value={chxBot.ref}/>
      </FormControl>
    case CHXBOT_VERSION__SPECIFIC:
      return <FormControl fullWidth>
        <InputLabel htmlFor='chxbot-version-number'>Number</InputLabel>
        <Input id='chxbot-version-number' value={chxBot.number}/>
      </FormControl>
    default:
      return null
  }
}
