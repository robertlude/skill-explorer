// External Dependencies

import { MenuItem } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Module

export default ({label, path, value, classes}) => {
  const splitPath = path.split('*')

      , displayPath = splitPath.length == 2
                    ? <span>{splitPath[0]}<span className={classes.glob}>*</span>{splitPath[1]}</span>
                    : splitPath[0]

  return <MenuItem value={value}>
    <span className={classes.label}>{label}</span>
    &nbsp;-&nbsp;
    {displayPath}
  </MenuItem>
}
