// Module

export default (adder, onAdd) => event => {
  onAdd(adder.state.newItem.trim())

  adder.setState({newItem: ''})
}
