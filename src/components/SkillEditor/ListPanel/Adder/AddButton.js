// External Dependencies

import AddIcon from '@material-ui/icons/Add'

import {
  InputAdornment,
  IconButton,
} from '@material-ui/core'

import React, {
  Component,
  createRef,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class AddButton extends Component {
  constructor(props) {
    super(props)

    this.button = createRef()
  }

  click() {
    this.button.current.click()
  }

  render() {
    const { button } = this
        , {
            disabled,
            onClick,
          } = this.props

    return <InputAdornment position='end'>
      <IconButton
        disabled={disabled}
        onClick ={onClick}
        ref     ={button}
      >
        <AddIcon />
      </IconButton>
    </InputAdornment>
  }
}

export default wrap()(AddButton)
