// External Dependencies

import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'

import {
  Box,
  IconButton,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class Item extends Component {
  render() {
    const {
            item,
            onDelete,
          } = this.props

    return <ListItem>
      <ListItemText>
        <Box fontFamily='Monospace'>
          {item}
        </Box>
      </ListItemText>
      <ListItemSecondaryAction>
        <IconButton
          edge   ='end'
          onClick={event => onDelete(item)}
        >
          <DeleteOutlinedIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  }
}

export default wrap()(Item)
