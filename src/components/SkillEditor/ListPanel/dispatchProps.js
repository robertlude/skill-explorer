// Internal Dependencies

import {
  deleteSkillAttribute,
  setSkillAttribute,
} from '@/actions'

// Module

export default {
  deleteSkillAttribute,
  setSkillAttribute,
}
