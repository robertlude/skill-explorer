// Module

export default panel => item => {
  const trimmedItem = item.trim()
      , {
          entry,
          attribute,
          optional,
          setSkillAttribute,
          deleteSkillAttribute,
        } = panel.props

      , newList = entry
                    .contents[attribute]
                    .filter(extantItem => extantItem != trimmedItem)

  if (optional && newList.length == 0) {
    deleteSkillAttribute(entry.path, attribute)
    return
  }

  setSkillAttribute(entry.path, attribute, newList)
}
