// External Dependencies

import {
  FormControl,
  Input,
  InputLabel,
} from '@material-ui/core'

import React, {
  Component,
  createRef,
} from 'react'

// Internal Dependencies

import AddButton      from './Adder/AddButton'
import addItem        from './Adder/addItem'
import catchReturnKey from './Adder/catchReturnKey'
import styles         from './Adder/styles'

import updateState from 'c/updateState'
import wrap        from 'c/wrap'

// Module

class Adder extends Component {
  constructor(props) {
    super(props)

    this.addButton = createRef()
    this.state     = { newItem: '' }
  }

  render() {
    const { addButton } = this
        , { newItem }   = this.state
        , {
            classes,
            itemType,
            onAdd,
          } = this.props

        , adornment = <AddButton
                        disabled={newItem.trim() == ''}
                        onClick ={addItem(this, onAdd)}
                        ref     ={addButton}
                      />

    return <FormControl fullWidth>
      <InputLabel htmlFor='new-item'>New {itemType}</InputLabel>
      <Input
        className   ={classes.monospace}
        endAdornment={adornment}
        id          ='new-item'
        onChange    ={updateState(this, 'newItem')}
        onKeyDown   ={catchReturnKey(addButton)}
        value       ={newItem}
      />
    </FormControl>
  }
}

export default wrap({styles})(Adder)
