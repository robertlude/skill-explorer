// External Dependencies

import _ from 'lodash'

// Module

export default panel => item => {
  const {
          entry,
          attribute,
          setSkillAttribute,
        } = panel.props

      , newList     = _.cloneDeep(entry.contents[attribute] || [])
      , trimmedItem = item.trim()

  if (newList.includes(trimmedItem)) return

  newList.push(trimmedItem)

  setSkillAttribute(entry.path, attribute, newList)
}
