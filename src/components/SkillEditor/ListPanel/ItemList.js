// External Dependencies

import { List } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Item from './ItemList/Item'

import wrap from 'c/wrap'

// Module

class ItemList extends Component {
  render() {
    const {
            items,
            listItemComponent,
            onDelete,
          } = this.props

        , ItemComponent = listItemComponent || Item

    return <List dense>
      {(items || []).sort().map(
        item => <ItemComponent
                  key     ={item}
                  item    ={item}
                  onDelete={onDelete}
                />
      )}
    </List>
  }
}

export default wrap()(ItemList)
