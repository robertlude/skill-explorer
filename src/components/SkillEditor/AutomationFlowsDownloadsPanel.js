// External Dependencies

import _ from 'lodash'

import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Filters             from './AutomationFlowsDownloadsPanel/Filters'
import createFilterUpdater from './AutomationFlowsDownloadsPanel/createFilterUpdater'
import filterDownloads     from './AutomationFlowsDownloadsPanel/filterDownloads'

import wrap from 'c/wrap'

import Panel from './Panel'

// Module

class AutomationFlowsDownloadsPanel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      filters: {
        branch: '*',
        path:   '',
        type:   '*',
      },
    }
  }

  render() {
    const { entry }   = this.props
        , { filters } = this.state

        , downloads = entry.contents.automationFlowsDownloads

        , filteredDownloads = filterDownloads(downloads, filters)

        , branches = _.uniq(downloads.map(download => download.branch))
        , types    = _.uniq(downloads.map(download => download.type))

    return <Panel title='Automation Flows Downloads'>
      <TableContainer>
        <Table size='small'>
          <Filters
            branches={branches}
            filters ={filters}
            onChange={createFilterUpdater(this)}
            types   ={types}
          />
          <TableBody>
            {filteredDownloads.map(download => (
              <TableRow key={download.path}>
                <TableCell>
                  <Box fontFamily='Monospace'>{download.type}</Box>
                </TableCell>
                <TableCell>
                  <Box fontFamily='Monospace'>{download.branch}</Box>
                </TableCell>
                <TableCell>
                  <Box fontFamily='Monospace'>{download.path}</Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Panel>
  }
}

export default wrap()(AutomationFlowsDownloadsPanel)
