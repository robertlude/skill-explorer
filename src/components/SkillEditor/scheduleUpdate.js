// Module

const UPDATE_TIMEOUT = 1000
    , pendingUpdates = {}

export default (editor, path, attribute, value) => {
  if (!pendingUpdates[path])
    pendingUpdates[path] = {}

  if (pendingUpdates[path][attribute])
    clearTimeout(pendingUpdates[path][attribute])

  const stateUpdate = {}

  stateUpdate[attribute] = value

  editor.setState(stateUpdate)

  pendingUpdates[path][attribute] = setTimeout(
    () => {
      editor.props.setSkillAttribute(path, attribute, value)

      delete pendingUpdates[path][attribute]
    },
    UPDATE_TIMEOUT,
  )
}
