// External Dependencies

import AddIcon    from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'

import React, {
  Component,
  createRef,
} from 'react'

// Internal Dependencies

import Adder         from './ListPanel/Adder'
import ItemList      from './ListPanel/ItemList'
import addItem       from './ListPanel/addItem'
import deleteItem    from './ListPanel/deleteItem'
import dispatchProps from './ListPanel/dispatchProps'
import styles        from './ListPanel/styles'

import wrap from 'c/wrap'

import Panel from './Panel'

// Module

class ListPanel extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
            attribute,
            classes,
            entry,
            itemType,
            listItemComponent,
            title,
          } = this.props

    return <Panel title={title}>
      <Adder
        itemType={itemType}
        onAdd   ={addItem(this)}
      />
      <ItemList
        items            ={entry.contents[attribute]}
        onDelete         ={deleteItem(this)}
        listItemComponent={listItemComponent}
      />
    </Panel>
  }
}

export default wrap({
  dispatchProps,
  styles,
})(ListPanel)
