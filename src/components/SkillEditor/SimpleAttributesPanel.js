// External Dependencies

import path from 'path'

import React, {
  Component,
} from 'react'

import {
  Box,
  Grid,
  TextField,
} from '@material-ui/core'

// Internal Dependencies

import dispatchProps from './SimpleAttributesPanel/dispatchProps'
import styles        from './SimpleAttributesPanel/styles'

import Panel         from './Panel'
import createUpdater from './createUpdater'

import wrap from 'c/wrap'

// Module

class SimpleAttributesPanel extends Component {
  constructor(props) {
    super(props)

    const {
            ami,
            command,
            commandEnvPath,
            description,
            env,
            name,
            type,
          } = this.props.entry.contents

    this.state = {
                   ami,
                   command,
                   commandEnvPath,
                   description,
                   env,
                   name,
                   type,
                 }
  }

  render() {
    const {
            classes,
            entry,
          } = this.props
        , {
            ami,
            command,
            commandEnvPath,
            description,
            env,
            name,
            type,
          } = this.state

        , update = key => createUpdater(this, entry.path, key)

    return <Panel
        title={
          <Box fontFamily='Monospace'>
            {path.basename(entry.path)}
          </Box>
        }
      >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            label   ='Name'
            onChange={update('name')}
            value   ={name}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            label   ='Description'
            onChange={update('description')}
            rows    ={1}
            rowsMax ={10}
            value   ={description}
            fullWidth
            multiline
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label   ='Command Environment Path'
            onChange={update('commandEnvPath')}
            value   ={commandEnvPath}
            fullWidth
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label     ='Environment'
            onChange  ={update('env')}
            value     ={env}
            fullWidth
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label     ='Type'
            onChange  ={update('type')}
            value     ={type}
            fullWidth
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label     ='AMI'
            onChange  ={update('ami')}
            value     ={ami}
            fullWidth
          />
        </Grid>
      </Grid>
    </Panel>
  }
}

export default wrap({
  dispatchProps,
  styles,
})(SimpleAttributesPanel)
