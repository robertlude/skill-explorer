// External Dependencies

import { TextField } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './CommandLinePanel/dispatchProps'
import splitLines    from './CommandLinePanel/splitLines'

import Panel         from './Panel'
import createUpdater from './createUpdater'

import wrap from 'c/wrap'

// Module

class CommandLinePanel extends Component {
  constructor(props) {
    super(props)

    const { command } = this.props.entry.contents

    this.state = { command }
  }

  render() {
    const { command } = this.state
        , { entry   } = this.props

        , update = (key, transform) => createUpdater(this, entry.path, key, transform)

    return <Panel title='Command Line'>
      <TextField
        label   ='Command Line'
        onChange={update('command', splitLines)}
        rows    ={1}
        rowsMax ={10}
        value   ={command.join('\n')}
        fullWidth
        multiline
      />
    </Panel>
  }
}

export default wrap({dispatchProps})(CommandLinePanel)
