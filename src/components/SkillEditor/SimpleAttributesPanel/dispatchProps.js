// Internal Dependencies

import { setSkillAttribute } from '@/actions'

// Module

export default { setSkillAttribute }
