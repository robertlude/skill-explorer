// Module

export default theme => ({
  paper: {
    flexGrow: 1,
    padding:  theme.spacing(3, 2),
  },
})
