// Module

export default (component, attribute) => event => {
  const stateUpdate = {}

  stateUpdate[attribute] = event.target.value

  component.setState(stateUpdate)
}
