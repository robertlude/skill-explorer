// External Dependencies

import React, {
  Component,
} from 'react'

// Internal Dependencies

import styles from './Background/styles'

import wrap from 'c/wrap'

// Module

class Background extends Component {
  render() {
    const { classes } = this.props

    return <div className={classes.background}>
      <div className={classes.omega}>
        &Omega;
      </div>
    </div>
  }
}

export default wrap({styles})(Background)
