// Module

export default theme => ({
  background: {
    position: 'fixed',
    left:     0,
    top:      0,
    bottom:   0,
    right:    0,

    zIndex: -1,

    background: `linear-gradient(0deg, rgba(0,0,0,0.5) 0%, rgba(0,0,0,100%) 20%),
                 linear-gradient(0deg, ${theme.palette.primary.main} 0%, ${theme.palette.primary.dark} 10%, ${theme.palette.secondary.dark} 15%, #000 20%)`,
  },
  omega: {
    position: 'fixed',
    left:     '50%',
    top:      '50%',

    transform: 'translate(-50%, -50%)',

    color:      '#000',
    fontFamily: 'sans-serif',
    fontSize:   '288pt',
    textShadow: `0 0 30px ${theme.palette.primary.main}`,
  },
})
