// External Dependencies

import { createRef } from 'react'

// Module

const newLine = /(\r\n|\n)/g

export default (component, names) =>
  names
    .replace(newLine, ' ')
    .split(' ')
    .filter(name => name.length)
    .forEach(name => component[name] = createRef())
