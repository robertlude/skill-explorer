// External Dependencies

import SettingsIcon from '@material-ui/icons/Settings'
import SaveIcon     from '@material-ui/icons/Save'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Form          from './Settings/Form'
import createUpdater from './Settings/createUpdater'
import dispatchProps from './Settings/dispatchProps'
import saveAndClose  from './Settings/saveAndClose'
import stateProps    from './Settings/stateProps'
import styles        from './Settings/styles'

import Modal       from 'c/Modal'
import createRefs  from 'c/createRefs'
import updateState from 'c/updateState'
import wrap        from 'c/wrap'

// Module

class Settings extends Component {
  constructor(props) {
    super(props)

    const { oliveSkillsPath } = this.props

    createRefs(this, 'modal')

    this.state = { oliveSkillsPath }
  }

  render() {
    const { modal }           = this
        , { oliveSkillsPath } = this.state
        , {
            classes,
            modalId,
          } = this.props

    return <Modal
      confirmIcon={<SaveIcon />}
      confirmText='Save'
      modalId    ={modalId}
      onCancel   ={() => modal.current.close()}
      onConfirm  ={saveAndClose(this)}
      paperClass ={classes.paper}
      ref        ={modal}
      titleIcon  ={<SettingsIcon />}
      titleText  ='Settings'
    >
      <Form
        oliveSkillsPath={oliveSkillsPath}
        onChange       ={createUpdater(this)}
      />
    </Modal>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  styles,
})(Settings)
