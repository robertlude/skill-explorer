// External Dependencies

import { connect } from 'react-redux'

// Module

export default ({dispatchProps, stateProps, forwardRef}) => component =>
  stateProps || dispatchProps
  ? connect(
      stateProps,
      dispatchProps,
      undefined,
      { forwardRef }
    )(component)
  : component
