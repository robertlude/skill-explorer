// External Dependencies

import { withStyles } from '@material-ui/core/styles'

// Module

export default ({styles}) => component =>
  styles
  ? withStyles(styles)(component)
  : component
