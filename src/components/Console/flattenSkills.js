// Internal Dependencies

import { FILE_FORMAT__SKILL } from '@/fileFormats'

// Module

const flattenSkills = entries =>
  entries
    .flatMap(entry => {
      if (entry.type == ENTRY_TYPE__DIRECTORY)
        return flattenSkills(entry.entries)

      if (  entry.type   != ENTRY_TYPE__FILE
         && entry.format == FILE_FORMAT__SKILL
         ) return entry

      return ENTRY_TYPE__IGNORE
    })
    .filter(entry => entry != ENTRY_TYPE__IGNORE)

export default flattenSkills
