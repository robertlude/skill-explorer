// Module

export default class FakeConsole {
  constructor() {
    this.lines = []
  }

  log() {
    this.lines.push(
      [...arguments]
        .map(value =>
          typeof value == 'string'
          ? value
          : JSON.stringify(value, undefined, 2)
        )
        .join(' ')
    )
  }
}
