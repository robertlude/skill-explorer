// External Dependencies

import _ from 'lodash'

// Internal Dependencies

import FakeConsole from './runCode/FakeConsole'
import runner      from './runCode/runner'

import getAllSkillEntryFiles  from '@/getAllSkillEntryFiles'
import { FILE_FORMAT__SKILL } from '@/fileFormats'

// Module

export default component => event => {
  const allFiles    = getAllSkillEntryFiles(component.props.skills)
      , fakeConsole = new FakeConsole()
      , sourceCode  = component.state.code

      , skills = allFiles.filter(file => file.format == FILE_FORMAT__SKILL)

      , context = { skills }

  runner(context, fakeConsole, sourceCode)

  component.setState({output: fakeConsole.lines.join('\n')})
}
