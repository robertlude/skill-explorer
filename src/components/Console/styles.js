// Module

export default theme => ({
  container: {
    position: 'fixed',
    left:     '5%',
    top:      '5%',
    right:    '5%',
    bottom:   '5%',
  },
  main: {
    height: '100%',
  },
  output: {
    textAlign:  'left',
    fontFamily: 'Monospace',
  },
})
