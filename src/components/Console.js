// External Dependencies

import AceEditor       from 'react-ace'
import CodeIcon        from '@material-ui/icons/Code'
import PlayArrowIcon   from '@material-ui/icons/PlayArrow'
import { ipcRenderer } from 'electron'

import 'ace-builds/src-noconflict/mode-javascript'
import 'ace-builds/src-noconflict/theme-solarized_dark'

import {
  Grid,
  Typography,
} from '@material-ui/core'

import React, {
  Component,
  createRef,
} from 'react'

// Internal Dependencies

import dispatchProps from './Console/dispatchProps'
import flattenSkills from './Console/flattenSkills'
import runCode       from './Console/runCode'
import stateProps    from './Console/stateProps'
import styles        from './Console/styles'

import Modal      from 'c/Modal'
import createRefs from 'c/createRefs'
import wrap       from 'c/wrap'

import { IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED } from '@/ipcEventTypes'

// Module

class Console extends Component {
  constructor(props) {
    super(props)

    createRefs(this, `modal`)

    const { skills } = this.props

    ipcRenderer.send(IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED)

    this.state = {
      code:   '',
      output: '',
    }
  }

  render() {
    const { modal } = this
        , {
            code,
            output,
          } = this.state
        , {
            classes,
            hideModal,
            modalId,
          } = this.props

    return <Modal
      confirmIcon={<PlayArrowIcon />}
      confirmText='Run'
      cancelText ='Close'
      modalId    ={modalId}
      onCancel   ={() => modal.current.close()}
      onConfirm  ={runCode(this)}
      paperClass ={classes.container}
      ref        ={modal}
      titleIcon  ={<CodeIcon />}
      titleText  ='Console'
    >
      <Grid
        className={classes.main}
        spacing  ={2}
        container
      >
        <Grid item xs={6}>
          <AceEditor
            height         ='100%'
            mode           ='javascript'
            onChange       ={newCode => this.setState({code: newCode})}
            placeholder    ='Code'
            showPrintMargin={false}
            tabSize        ={2}
            theme          ='solarized_dark'
            value          ={code}
            width          ='100%'
          />
        </Grid>
        <Grid item xs={6} className={classes.output}>
          <AceEditor
            height         ='100%'
            placeholder    ='Output'
            showPrintMargin={false}
            theme          ='solarized_dark'
            value          ={output}
            width          ='100%'
            readOnly
          />
        </Grid>
      </Grid>
    </Modal>
  }
}

export default wrap({
  dispatchProps,
  stateProps,
  styles,
})(Console)
