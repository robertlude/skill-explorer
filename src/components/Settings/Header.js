// External Dependencies

import SettingsIcon from '@material-ui/icons/Settings'

import {
  Grid,
  Typography,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class Header extends Component {
  render() {
    return <Grid
      alignItems='center'
      spacing   ={2}
      container
    >
      <Grid item>
        <SettingsIcon />
      </Grid>
      <Grid item>
        <Typography
          component='h2'
          variant  ='h5'
        >
          Settings
        </Typography>
      </Grid>
    </Grid>
  }
}

export default wrap()(Header)
