// Module

export default theme => ({
  paper: {
    position:  'fixed',
    left:      '50%',
    top:       '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'left',
    width:     400,
    height:    225,
  },
})
