// Internal Dependencies

import {
  disableUI,
  enableUI,
} from '@/actions'

// Module

export default {
  disableUI,
  enableUI,
}
