// External Dependencies

import { remote } from 'electron'

const { dialog } = remote

// Module

export default component => event => {
  const {
          disableUI,
          enableUI,
          onChange,
        } = component.props

  disableUI('Select a directory in the file dialog to continue')

  dialog
    .showOpenDialog({properties: ['openDirectory']})
    .then(result => {
      enableUI()

      if (result.cancelled) return

      onChange(result.filePaths[0])
    })
}
