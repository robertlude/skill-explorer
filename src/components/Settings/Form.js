// External Dependencies

import FolderOpenIcon from '@material-ui/icons/FolderOpen'

import {
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps         from './Form/dispatchProps'
import selectOliveSkillsPath from './Form/selectOliveSkillsPath'

import cancelEvent    from 'c/cancelEvent'
import wrap           from 'c/wrap'

// Module

class Form extends Component {
  render() {
    const {
            oliveSkillsPath,
            onChange,
          } = this.props

    return <div>
      <FormControl
        margin='normal'
        fullWidth
      >
        <InputLabel htmlFor='olive-skills-path'>Olive Skills path</InputLabel>
        <Input
          id      ='olive-skills-path'
          type    ='text'
          value   ={oliveSkillsPath}
          onChange={event => onChange(event.target.value)}
          endAdornment={
            <InputAdornment position='end'>
              <IconButton
                onClick    ={selectOliveSkillsPath(this)}
                onMouseDown={cancelEvent()}
              >
                <FolderOpenIcon />
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    </div>
  }

  get data() {
    return {
      oliveSkillsPath: this.state.oliveSkillsPath
    }
  }
}

export default wrap({
  forwardRef: true,
  dispatchProps,
})(Form)
