// Internal Dependencies

import {
  hideModalComplete,
  setOliveSkillsPath,
} from '@/actions'

// Module

export default {
  hideModalComplete,
  setOliveSkillsPath,
}
