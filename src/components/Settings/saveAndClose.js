// External Dependencies

import { ipcRenderer } from 'electron'

// Internal Dependencies

import { IPC_EVENT_TYPE__GET_SKILL_ENTRIES } from '@/ipcEventTypes'

// Module

export default settings => event => {
  const { modal } = settings
      , {
          modalId,
          setOliveSkillsPath,
        } = settings.props

      , { oliveSkillsPath } = settings.state

  setOliveSkillsPath(oliveSkillsPath)

  modal.current.close()

  ipcRenderer.send(IPC_EVENT_TYPE__GET_SKILL_ENTRIES)
}
