// External Dependencies

import SaveIcon from '@material-ui/icons/Save'

import {
  Button,
  Grid,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Module

class Buttons extends Component {
  render() {
    const {
            onCancel,
            onSave,
          } = this.props

    return <Grid
      justify='flex-end'
      spacing={2}
      container
    >
      <Grid item>
        <Button onClick={onCancel}>Cancel</Button>
      </Grid>
      <Grid item>
        <Button
          onClick  ={onSave}
          color    ='primary'
          startIcon={<SaveIcon />}
          variant  ='contained'
        >
          Save
        </Button>
      </Grid>
    </Grid>
  }
}

export default wrap()(Buttons)
