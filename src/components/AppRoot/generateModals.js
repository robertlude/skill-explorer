// External Dependencies

import React from 'react'

// Internal Dependencies

import modalMap from './generateModals/modalMap'

// Module

export default modals => modals.map(modal => {
  const Component = modalMap[modal.type]

  return <Component
           key    ={modal.id}
           modalId={modal.id}
         />
})
