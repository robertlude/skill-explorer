// External Dependencies

import React from 'react'

// Internal Dependencies

import UIDisabler from 'c/UIDisabler'

// Module

export default disabler => {
  return null

  const {
          fading,
          message,
          visible,
        } = disabler

  if (!visible) return null

  return <UIDisabler
    animateClose={fading}
    message     ={message}
  />
}
