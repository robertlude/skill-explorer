// Internal Dependencies

import Console  from 'c/Console'
import Settings from 'c/Settings'

import {
  MODAL_TYPE__CONSOLE,
  MODAL_TYPE__SETTINGS,
} from '@/modalTypes'

// Module

export default {
  [MODAL_TYPE__CONSOLE]:  Console,
  [MODAL_TYPE__SETTINGS]: Settings,
}
