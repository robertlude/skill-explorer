// External Dependencies

import { Grid }    from '@material-ui/core'
import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import AutomationFlowsDownloadsPanel from './SkillEditor/AutomationFlowsDownloadsPanel'
import ChxBotPanel                   from './SkillEditor/ChxBotPanel'
import CommandLinePanel              from './SkillEditor/CommandLinePanel'
import DownloadListItem              from './SkillEditor/DownloadListItem'
import ListPanel                     from './SkillEditor/ListPanel'
import SimpleAttributesPanel         from './SkillEditor/SimpleAttributesPanel'

import wrap from 'c/wrap'

// Module

class SkillEditor extends Component {
  render() {
    const {
      entry,
    } = this.props

    return <Grid container spacing={2}>
      <Grid item xs={12}>
        <SimpleAttributesPanel
          entry={entry}
          key  ={`${entry.path}-simple-attributes`}
        />
      </Grid>
      <Grid item xs={12}>
        <AutomationFlowsDownloadsPanel entry={entry} />
      </Grid>
      <Grid item xs={6}>
        <ChxBotPanel entry={entry} />
      </Grid>
      <Grid item xs={6}>
        <CommandLinePanel
          entry={entry}
          key  ={`${entry.path}-command-line`}
        />
      </Grid>
      <Grid item xs={6}>
        <ListPanel
          attribute        ='otherDownloads'
          entry            ={entry}
          itemType         ='other download'
          listItemComponent={DownloadListItem}
          title            ='Other Downloads'
        />
      </Grid>
      <Grid item xs={6}>
        <ListPanel
          attribute='outputGlobs'
          entry    ={entry}
          itemType ='output glob'
          title    ='Output Globs'
        />
      </Grid>
      <Grid item xs={6}>
        <ListPanel
           attribute='metricsLogs'
           entry    ={entry}
           itemType ='metrics log'
           title    ='Metrics Logs'
           optional
        />
      </Grid>
    </Grid>
  }
}

export default wrap()(SkillEditor)
