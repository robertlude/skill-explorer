// Module

export default theme => ({
  container: {
    display:       'flex',
    flexDirection: 'column',

    position: 'absolute',
    top:      0,
    bottom:   0,

    padding: theme.spacing(2),
  },
  content: {
    flex:         '1 1 auto',
    position:     'relative',
    marginTop:    theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  footer: {
    flex: '0 0 auto',
  },
  header: {
    flex: '0 0 auto',
  },
})
