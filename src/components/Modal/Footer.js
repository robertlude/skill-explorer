// External Dependencies

import {
  Button,
  Grid,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './Footer/dispatchProps'

import wrap from 'c/wrap'

// Module

class Footer extends Component {
  render() {
    const {
            cancelIcon,
            cancelText,
            confirmIcon,
            confirmText,
            hideModal,
            modalId,
            onCancel,
            onConfirm,
          } = this.props

        , closer = () => hideModal(modalId)

    return <Grid
      justify='flex-end'
      spacing={2}
      container
    >
      <Grid item>
        <Button
          onClick  ={onCancel || closer}
          startIcon={cancelIcon}
        >
          {cancelText || 'Cancel'}
        </Button>
      </Grid>
      <Grid item>
        <Button
          color    ='primary'
          onClick  ={onConfirm || closer}
          startIcon={confirmIcon}
          variant  ='contained'
        >
          {confirmText || 'Ok'}
        </Button>
      </Grid>
    </Grid>
  }
}

export default wrap({dispatchProps})(Footer)
