// Internal Dependencies

import { hideModal } from '@/actions'

// Module

export default { hideModal }
