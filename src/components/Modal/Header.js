// External Dependencies

import {
  Grid,
  Typography,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import wrap from 'c/wrap'

// Component

class Header extends Component {
  render() {
    const {
            icon,
            title,
          } = this.props

    return <Grid
      alignItems='center'
      spacing   ={2}
      container
    >
      <Grid item>
        {icon}
      </Grid>
      <Grid item>
        <Typography
          component='h2'
          variant  ='h5'
        >
          {title}
        </Typography>
      </Grid>
    </Grid>
  }
}

export default wrap()(Header)
