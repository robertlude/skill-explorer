// External Dependencies

import React, {
  Component,
} from 'react'

import {
  Drawer,
  Toolbar,
} from '@material-ui/core'

// Internal Dependencies

import Header       from './Skills/Header'
import buildContent from './Skills/buildContent'
import stateProps   from './Skills/stateProps'
import styles       from './Skills/styles'

import SkillList   from 'c/SkillList'
import wrap        from 'c/wrap'

import findEntry from '@/findEntry'

// Module

class Skills extends Component {
  render() {
    const {
            classes,
            showModal,
            skills,
          } = this.props

        , {
            entries,
            selectedFile,
          } = skills

        , selectedEntry  = findEntry(entries, selectedFile)

        , content = buildContent(selectedEntry)

    return <div className={classes.root}>
      <Header />
      <Drawer
        variant  ='permanent'
        className={classes.drawer}
        classes  ={{paper: classes.drawerPaper}}
      >
        <Toolbar variant='dense' />
        <SkillList entries={entries} />
      </Drawer>
      <main className={classes.content}>
        <Toolbar variant='dense' />
        {content}
      </main>
    </div>
  }
}

export default wrap({
  stateProps,
  styles,
})(Skills)
