// Module

const ACTION_TYPE__DELETE_SKILL_ATTRIBUTE = 'actionType:deleteSkillAttribute'
    , ACTION_TYPE__DISABLE_UI             = 'actionType:disableUI'
    , ACTION_TYPE__ENABLE_UI              = 'actionType:enableUI'
    , ACTION_TYPE__ENABLE_UI_COMPLETE     = 'actionType:enableUIComplete'
    , ACTION_TYPE__HIDE_MODAL             = 'actionType:hideModal'
    , ACTION_TYPE__HIDE_MODAL_COMPLETE    = 'actionType:hideModalComplete'
    , ACTION_TYPE__REDO                   = 'actionType:redo'
    , ACTION_TYPE__SELECT_FILE            = 'actionType:selectFile'
    , ACTION_TYPE__SET_FILE_CONTENTS      = 'actionType:setFileContents'
    , ACTION_TYPE__SET_FILTER             = 'actionType:setFilter'
    , ACTION_TYPE__SET_OLIVE_SKILLS_PATH  = 'actionType:setOliveSkillsPath'
    , ACTION_TYPE__SET_SKILL_ATTRIBUTE    = 'actionType:setSkillAttribute'
    , ACTION_TYPE__SET_SKILL_ENTRIES      = 'actionType:setSkillEntries'
    , ACTION_TYPE__SHOW_MODAL             = 'actionType:showModal'
    , ACTION_TYPE__TOGGLE_DIRECTORY       = 'actionType:toggleDirectory'
    , ACTION_TYPE__UNDO                   = 'actionType:undo'

export {
  ACTION_TYPE__DELETE_SKILL_ATTRIBUTE,
  ACTION_TYPE__DISABLE_UI,
  ACTION_TYPE__ENABLE_UI,
  ACTION_TYPE__ENABLE_UI_COMPLETE,
  ACTION_TYPE__HIDE_MODAL,
  ACTION_TYPE__HIDE_MODAL_COMPLETE,
  ACTION_TYPE__REDO,
  ACTION_TYPE__SELECT_FILE,
  ACTION_TYPE__SET_FILE_CONTENTS,
  ACTION_TYPE__SET_FILTER,
  ACTION_TYPE__SET_OLIVE_SKILLS_PATH,
  ACTION_TYPE__SET_SKILL_ATTRIBUTE,
  ACTION_TYPE__SET_SKILL_ENTRIES,
  ACTION_TYPE__SHOW_MODAL,
  ACTION_TYPE__TOGGLE_DIRECTORY,
  ACTION_TYPE__UNDO,
}
