// External Dependencies

import React           from 'react'
import ReactDOM        from 'react-dom'
import { Provider }    from 'react-redux'
import { ipcRenderer } from 'electron'

import {
  applyMiddleware,
  createStore,
} from 'redux'

// Internal Dependencies

import AppRoot from 'c//AppRoot'

import actionLogger             from '@/actionLogger'
import reducers                 from '@/reducers'
import { MODAL_TYPE__SETTINGS } from '@/modalTypes'

import {
  setOliveSkillsPath,
  showModal,
} from '@/actions'

import {
  IPC_EVENT_TYPE__EXECUTE_ACTION,
  IPC_EVENT_TYPE__GET_INITIAL_STATE,
  IPC_EVENT_TYPE__GET_SKILL_ENTRIES,
  IPC_EVENT_TYPE__SET_INITIAL_STATE,
} from '@/ipcEventTypes'

// Module

const settingsAreValid = settings => (settings.oliveSkillsPath || '').trim() != ''

const sendActionToMain = store => next => action => {
  if (!action.fromMain)
    ipcRenderer.send(IPC_EVENT_TYPE__EXECUTE_ACTION, action)

  return next(action)
}

let store

ipcRenderer.on(IPC_EVENT_TYPE__EXECUTE_ACTION, (event, action) => {
  action.fromMain = true

  store.dispatch(action)
})

ipcRenderer.on(IPC_EVENT_TYPE__SET_INITIAL_STATE, (event, state) => {
  store = createStore(
    reducers,
    state,
    applyMiddleware(
      actionLogger,
      sendActionToMain,
    ),
  )

  if (!settingsAreValid(state.settings)) store.dispatch(showModal(MODAL_TYPE__SETTINGS))

  ReactDOM.render(
    <Provider store={store}>
      <AppRoot />
    </Provider>,
    document.getElementById('app-root')
  )

  ipcRenderer.send(IPC_EVENT_TYPE__GET_SKILL_ENTRIES)
})

ipcRenderer.send(IPC_EVENT_TYPE__GET_INITIAL_STATE)
