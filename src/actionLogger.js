// Module

export default store => next => action => {
  const result = next(action)

  console.log(
    action.type,
    {
      action,
      state: store.getState(),
    }
  )

  return result
}
