// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '@/entryTypes'

// Modules

const getAllSkillEntryFiles = entries =>
  entries
    .flatMap(entry =>
      entry.type == ENTRY_TYPE__DIRECTORY
      ? getAllSkillEntryFiles(entry.entries)
      : entry
    )

export default getAllSkillEntryFiles
