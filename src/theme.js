// External Dependencies

import cyan               from '@material-ui/core/colors/cyan'
import grey               from '@material-ui/core/colors/grey'
import purple             from '@material-ui/core/colors/purple'
import { createMuiTheme } from '@material-ui/core/styles'

// Module

export default createMuiTheme({
  palette: {
    primary:   purple,
    secondary: {
      main: '#651fff',
    },
    type:      'dark',
  },
  overrides: {
    MuiPaper: {
      root: {
        backdropFilter:  'blur(20px)',
        background:      `linear-gradient(0deg, ${grey[500]}50 0%, ${grey[500]}60 100%)`,
        backgroundColor: undefined,
        border:          '1px solid rgba(255,255,255,0.25)',
      },
    },
    MuiDrawer: {
      paper: {
        border: 'none',
      },
    },
    MuiAppBar: {
      root: {
        background: 'none',
        border:     'none',
      }
    },
  },
})
