// Module

const FILE_FORMAT__JSON    = 'fileFormat:json'
    , FILE_FORMAT__SKILL   = 'fileFormat:skill'
    , FILE_FORMAT__UNKNOWN = 'fileFormat:unknown'

export {
  FILE_FORMAT__JSON,
  FILE_FORMAT__SKILL,
  FILE_FORMAT__UNKNOWN,
}
