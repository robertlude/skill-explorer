// Internal Dependencies

import {
  ENTRY_TYPE__DIRECTORY,
  ENTRY_TYPE__FILE,
} from './entryTypes'

// Module

const findEntry = (entries, path) => {
  for (let entry of entries) {
    if (  entry.type == ENTRY_TYPE__FILE
       && entry.path == path
       ) return entry

    if (  entry.type == ENTRY_TYPE__DIRECTORY
       && path.startsWith(entry.path)
       ) {
      const result = findEntry(entry.entries, path)

      if (result) return result
    }
  }
}

export default findEntry
