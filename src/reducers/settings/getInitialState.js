// External Dependencies

import fs      from 'fs'
import path    from 'path'
import { app } from 'electron'

// Module

const initialState = {
  oliveSkillsPath: '',
}

export default () => {
  // if we're in the renderer process, we're not loading this data. it will be
  // fetched from the main process. (the renderer process is not allowed to
  // access fs)

  if (!app) return initialState

  const settingsPath = path.join(
                         app.getPath('userData'),
                         'settings.json',
                       )

  try {
    // if the file exists, return the data

    return JSON.parse(fs.readFileSync(settingsPath))
  } catch {
    // if the file does not exist, return the default initial state

    return initialState
  }
}
