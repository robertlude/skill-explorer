// External Dependencies

import fs      from 'fs'
import path    from 'path'
import { app } from 'electron'

// Module

export default settings => {
  // if we're in the renderer process, we're not saving this data. it will be
  // saved only in the main process. (the renderer process is not allowed to
  // access fs, nor do we need to save twice)

  if (!app) return

  const settingsPath = path.join(
                         app.getPath('userData'),
                         'settings.json',
                       )

  fs.writeFile(
    settingsPath,
    JSON.stringify(settings, undefined, 2),
    error => {
      if (error) console.log(`saveSettings error: ${error}`)
    },
  )
}
