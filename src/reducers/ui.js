// Internal Dependencies

import disableUI         from './ui/disableUI'
import enableUI          from './ui/enableUI'
import enableUIComplete  from './ui/enableUIComplete'
import hideModal         from './ui/hideModal'
import hideModalComplete from './ui/hideModalComplete'
import showModal         from './ui/showModal'

import generateReducer from './generateReducer'

import {
  ACTION_TYPE__DISABLE_UI,
  ACTION_TYPE__ENABLE_UI,
  ACTION_TYPE__ENABLE_UI_COMPLETE,
  ACTION_TYPE__HIDE_MODAL,
  ACTION_TYPE__HIDE_MODAL_COMPLETE,
  ACTION_TYPE__SHOW_MODAL,
} from '@/actionTypes'

// Module

const initialState = {
  disabler: {
    fading:  false,
    message: '',
    visible: false,
  },
  modals: [],
}

export default generateReducer(
  initialState,
  {
    [ACTION_TYPE__DISABLE_UI]:          disableUI,
    [ACTION_TYPE__ENABLE_UI]:           enableUI,
    [ACTION_TYPE__ENABLE_UI_COMPLETE]:  enableUIComplete,
    [ACTION_TYPE__HIDE_MODAL]:          hideModal,
    [ACTION_TYPE__HIDE_MODAL_COMPLETE]: hideModalComplete,
    [ACTION_TYPE__SHOW_MODAL]:          showModal,
  },
)
