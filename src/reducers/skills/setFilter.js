// Module

export default (state, action) => ({
  ...state,
  filter: action.filter,
})
