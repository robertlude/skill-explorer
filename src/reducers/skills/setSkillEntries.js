// Module

export default (state, action) => ({
  ...state,
  entries: action.entries,
})
