// External Dependencies

import { cloneDeep } from 'lodash'

// Internal Dependencies

import findEntry from '@/findEntry'
import tap       from '@/tap'

// Module

export default (state, {entries}) =>
  cloneDeep(state)::tap(state =>
    entries.forEach(({contents, format, path}) => {
      let entry = findEntry(state.entries, path)

      if (entry && !entry.loaded) {
        entry.contents = contents
        entry.format   = format
        entry.loaded   = true
        entry.modified = false
      }
    })
  )
