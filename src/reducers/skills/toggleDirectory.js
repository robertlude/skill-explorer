// External Dependencies

import { cloneDeep } from 'lodash'

// Internal Dependencies

import { ENTRY_TYPE__DIRECTORY } from '../../entryTypes'

// Module

const toggleRecursively = (entries, path) => {
  const result = !!entries.find(entry => {
    if (entry.type != ENTRY_TYPE__DIRECTORY) return false

    if (entry.path == path) {
      entry.expanded = !entry.expanded
      return true
    }

    if (toggleRecursively(entry.entries, path)) return true

    return false
  })

  return result
}

export default (state, action) => {
  const newState = cloneDeep(state)

  toggleRecursively(newState.entries, action.path)

  return newState
}
