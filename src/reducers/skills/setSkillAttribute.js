// External Dependencies

import uuid          from 'uuid/v4'
import { cloneDeep } from 'lodash'

// Internal Dependencies

import findEntry from '../../findEntry'

// Module

export default (state, {path, attribute, value}) => {
  const newState = cloneDeep(state)

      , entry = findEntry(newState.entries, path)

      , oldEntry = cloneDeep(entry)

  oldEntry.future = entry

  entry.past = oldEntry

  delete entry.future

  entry.contents[attribute] = value
  entry.modified            = true
  entry.revision            = uuid()

  return newState
}
