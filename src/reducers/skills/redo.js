// External Dependencies

import { cloneDeep } from 'lodash'

// Internal Dependencies

import findEntry    from '@/findEntry'
import replaceEntry from '@/replaceEntry'

// Module

export default (state, action) => {
  const newState = cloneDeep(state)

      , {
          entries,
          selectedFile,
        } = newState

      , entry = findEntry(entries, selectedFile)

  if (entry.future) replaceEntry(entries, entry.future)

  return newState
}
