// Module

export default (state, action) => ({
  ...state,
  selectedFile: action.path,
})
