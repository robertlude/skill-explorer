// External Dependencies

import _ from 'lodash'

// Internal Dependencies

import findEntry from '../../findEntry'

// Module

export default (state, {attribute, path}) => {
  const newState = _.cloneDeep(state)

      , entry = findEntry(newState.entries, path)

  delete entry.contents[attribute]
  entry.modified = true

  return newState
}
