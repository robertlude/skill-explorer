// Internal Dependencies

import getInitialState    from './settings/getInitialState'
import setOliveSkillsPath from './settings/setOliveSkillsPath'
import saveSettings       from './settings/saveSettings'

import generateReducer from './generateReducer'

import { ACTION_TYPE__SET_OLIVE_SKILLS_PATH } from '@/actionTypes'

// Module

export default generateReducer(
  getInitialState,
  {
    [ACTION_TYPE__SET_OLIVE_SKILLS_PATH]: setOliveSkillsPath,
  },
  saveSettings,
)
