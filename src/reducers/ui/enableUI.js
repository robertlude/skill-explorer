// Module

export default (state, action) => ({
  ...state,
  disabler: {
    ...state.disabler,
    fading: true,
  },
})
