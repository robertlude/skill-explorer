// Module

export default (state, action) => ({
  ...state,
  disabler: {
    fading:  false,
    message: '',
    visible: false,
  },
})
