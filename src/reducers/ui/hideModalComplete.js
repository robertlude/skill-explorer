// Module

export default (state, action) => ({
  ...state,
  modals: state.modals.filter(modal => modal.id != action.modalId),
})
