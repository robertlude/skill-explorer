// Module

export default (state, action) => ({
  ...state,
  modals: state.modals.map(modal =>
    modal.id != action.modalId
    ? modal
    : {
        ...modal,
        fading: true,
      }
  )
})
