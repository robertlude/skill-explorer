// External Dependencies

import _     from 'lodash'
import uuid4 from 'uuid/v4'

// Module

export default (state, action) => ({
  ...state,
  modals: _.concat(state.modals, {
    id:     uuid4(),
    fading: false,
    type:   action.modalType,
  }),
})
