// Module

export default (state, action) => ({
  ...state,
  disabler: {
    visible: true,
    message: action.message,
    fading:  false,
  },
})
