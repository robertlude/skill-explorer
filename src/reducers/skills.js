// Internal Dependencies

import deleteSkillAttribute from './skills/deleteSkillAttribute'
import redo                 from './skills/redo'
import selectFile           from './skills/selectFile'
import setFileContents      from './skills/setFileContents'
import setFilter            from './skills/setFilter'
import setSkillAttribute    from './skills/setSkillAttribute'
import setSkillEntries      from './skills/setSkillEntries'
import toggleDirectory      from './skills/toggleDirectory'
import undo                 from './Skills/undo'

import generateReducer from './generateReducer'

import {
  ACTION_TYPE__DELETE_SKILL_ATTRIBUTE,
  ACTION_TYPE__REDO,
  ACTION_TYPE__SELECT_FILE,
  ACTION_TYPE__SET_FILE_CONTENTS,
  ACTION_TYPE__SET_FILTER,
  ACTION_TYPE__SET_SKILL_ATTRIBUTE,
  ACTION_TYPE__SET_SKILL_ENTRIES,
  ACTION_TYPE__TOGGLE_DIRECTORY,
  ACTION_TYPE__UNDO,
} from '../actionTypes'

// Module

const initialState = {
  entries:      [],
  filter:       '',
  selectedFile: '',
}

export default generateReducer(
  initialState,
  {
    [ACTION_TYPE__DELETE_SKILL_ATTRIBUTE]: deleteSkillAttribute,
    [ACTION_TYPE__REDO]:                   redo,
    [ACTION_TYPE__SELECT_FILE]:            selectFile,
    [ACTION_TYPE__SET_FILE_CONTENTS]:      setFileContents,
    [ACTION_TYPE__SET_FILTER]:             setFilter,
    [ACTION_TYPE__SET_SKILL_ATTRIBUTE]:    setSkillAttribute,
    [ACTION_TYPE__SET_SKILL_ENTRIES]:      setSkillEntries,
    [ACTION_TYPE__TOGGLE_DIRECTORY]:       toggleDirectory,
    [ACTION_TYPE__UNDO]:                   undo,
  },
)
