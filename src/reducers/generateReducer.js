// External Dependencies

import _ from 'lodash'

// Module

export default (initialState, actionMap, onComplete) => {
  const actionMapKeys = Object.keys(actionMap)

  return (state, action) => {
    if (!state) {
      if (typeof initialState == 'function') {
        state = initialState()
      } else {
        state = _.cloneDeep(initialState)
      }
    }

    if (actionMapKeys.includes(action.type)) {
      let newState = actionMap[action.type](state, action)

      if (onComplete) onComplete(newState)

      return newState
    }

    return state
  }
}
