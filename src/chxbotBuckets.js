// Module

const CHXBOT_BUCKET__STANDARD = 'chxbotBucket:standard'
    , CHXBOT_BUCKET__OLD      = 'chxbotBucket:old'

export {
  CHXBOT_BUCKET__STANDARD,
  CHXBOT_BUCKET__OLD,
}
