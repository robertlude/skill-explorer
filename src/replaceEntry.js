// Internal Dependencies

import {
  ENTRY_TYPE__DIRECTORY,
  ENTRY_TYPE__FILE,
} from './entryTypes'

// Module

const replaceEntry = (entries, newEntry) => {
  debugValues('replaceEntry()', {
    entries,
    newEntry,
  })

  entries.forEach((entry, entryIndex) => {
    if (  entry.type == ENTRY_TYPE__FILE
       && entry.path == newEntry.path
       ) {
      entries[entryIndex] = newEntry

      console.log('entry replaced')

      return true
    }

    if (  entry.type == ENTRY_TYPE__DIRECTORY
       && newEntry.path.startsWith(entry.path)
       ) {
      if (replaceEntry(entry.entries, newEntry)) return true
    }
  })

  return false
}

export default replaceEntry
