// Module

const CHXBOT_VERSION__LATEST   = 'chxbotVersion:latest'
    , CHXBOT_VERSION__NIGHTLY  = 'chxbotVersion:nightly'
    , CHXBOT_VERSION__NONE     = 'chxbotVersion:none'
    , CHXBOT_VERSION__SNAPSHOT = 'chxbotVersion:snapshot'
    , CHXBOT_VERSION__SPECIFIC = 'chxbotVersion:specific'

export {
  CHXBOT_VERSION__LATEST,
  CHXBOT_VERSION__NIGHTLY,
  CHXBOT_VERSION__NONE,
  CHXBOT_VERSION__SNAPSHOT,
  CHXBOT_VERSION__SPECIFIC,
}
