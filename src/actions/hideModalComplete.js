// Internal Dependencies

import { ACTION_TYPE__HIDE_MODAL_COMPLETE } from '@/actionTypes'

// Module

export default modalId => ({
  type: ACTION_TYPE__HIDE_MODAL_COMPLETE,
  modalId,
})
