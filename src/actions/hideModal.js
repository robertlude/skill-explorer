// Internal Dependencies
import { ACTION_TYPE__HIDE_MODAL } from '@/actionTypes'

// Module

export default modalId => ({
  type: ACTION_TYPE__HIDE_MODAL,
  modalId,
})
