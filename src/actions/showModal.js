// Internal Dependencies

import { ACTION_TYPE__SHOW_MODAL } from '@/actionTypes'

// Module

export default modalType => ({
  type: ACTION_TYPE__SHOW_MODAL,
  modalType,
})
