// Internal Dependencies

import { ACTION_TYPE__UNDO } from '@/actionTypes'

// Module

export default () => ({type: ACTION_TYPE__UNDO})
