// Internal Dependencies

import { ACTION_TYPE__SET_SKILL_ATTRIBUTE } from '../actionTypes'

// Module

export default (path, attribute, value) => ({
  type: ACTION_TYPE__SET_SKILL_ATTRIBUTE,
  attribute,
  path,
  value,
})
