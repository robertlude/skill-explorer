// Internal Dependencies

import { ACTION_TYPE__TOGGLE_DIRECTORY } from '../actionTypes'

// Module

export default path => ({
  type: ACTION_TYPE__TOGGLE_DIRECTORY,
  path,
})
