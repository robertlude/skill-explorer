// Internal Dependencies

import { ACTION_TYPE__SET_SKILL_ENTRIES } from '../actionTypes'

// Module

export default entries => ({
  type: ACTION_TYPE__SET_SKILL_ENTRIES,
  entries,
})
