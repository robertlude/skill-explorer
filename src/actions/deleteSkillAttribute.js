// Internal Dependencies

import { ACTION_TYPE__DELETE_SKILL_ATTRIBUTE } from '../actionTypes'

// Module

export default (path, attribute) => ({
  type: ACTION_TYPE__DELETE_SKILL_ATTRIBUTE,
  attribute,
  path,
})
