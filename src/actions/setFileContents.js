// Internal Dependencies

import { ACTION_TYPE__SET_FILE_CONTENTS } from '../actionTypes'

// Module

export default entries => ({
  type: ACTION_TYPE__SET_FILE_CONTENTS,
  entries,
})
