// Internal Dependencies

import { ACTION_TYPE__SET_FILTER } from '@/actionTypes'

// Module

export default filter => ({
  type: ACTION_TYPE__SET_FILTER,
  filter,
})
