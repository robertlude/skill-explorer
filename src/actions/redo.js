// Internal Dependencies

import { ACTION_TYPE__REDO } from '@/actionTypes'

// Module

export default () => ({type: ACTION_TYPE__REDO})
