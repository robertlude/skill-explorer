// Internal Dependencies

import { ACTION_TYPE__SET_OLIVE_SKILLS_PATH } from '../actionTypes'

// Module

export default path => ({
  type: ACTION_TYPE__SET_OLIVE_SKILLS_PATH,
  path,
})
