// Internal Dependencies

import { ACTION_TYPE__SELECT_FILE } from '../actionTypes'

// Module

export default path => ({
  type: ACTION_TYPE__SELECT_FILE,
  path,
})
