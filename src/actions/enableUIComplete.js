// Internal Dependencies

import { ACTION_TYPE__ENABLE_UI_COMPLETE } from '@/actionTypes'

// Module

export default () => ({type: ACTION_TYPE__ENABLE_UI_COMPLETE})
