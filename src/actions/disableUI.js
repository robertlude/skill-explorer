// Internal Dependencies

import { ACTION_TYPE__DISABLE_UI } from '../actionTypes'

// Module

export default message => ({
  type: ACTION_TYPE__DISABLE_UI,
  message,
})
