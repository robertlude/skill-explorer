// External Dependencies

import { combineReducers } from 'redux'

// Internal Dependencies

import settings from './reducers/settings'
import skills   from './reducers/skills'
import ui       from './reducers/ui'

// Module

export default combineReducers({
  settings,
  skills,
  ui,
})
