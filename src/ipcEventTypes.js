// Module

const IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED = 'ipcEventType:ensureAllFilesLoaded'
    , IPC_EVENT_TYPE__ENSURE_FILES_LOADED     = 'ipcEventType:ensureFilesLoaded'
    , IPC_EVENT_TYPE__EXECUTE_ACTION          = 'ipcEventType:executeAction'
    , IPC_EVENT_TYPE__GET_INITIAL_STATE       = 'ipcEventType:getInitialState'
    , IPC_EVENT_TYPE__GET_SKILL_ENTRIES       = 'ipcEventType:getSkillEntries'
    , IPC_EVENT_TYPE__SET_INITIAL_STATE       = 'ipcEventType:setInitialState'

export {
  IPC_EVENT_TYPE__ENSURE_ALL_FILES_LOADED,
  IPC_EVENT_TYPE__ENSURE_FILES_LOADED,
  IPC_EVENT_TYPE__EXECUTE_ACTION,
  IPC_EVENT_TYPE__GET_INITIAL_STATE,
  IPC_EVENT_TYPE__GET_SKILL_ENTRIES,
  IPC_EVENT_TYPE__SET_INITIAL_STATE,
}
