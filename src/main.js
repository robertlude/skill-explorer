// Internal Dependencies

import generateStore  from './main/generateStore'
import setupAppEvents from './main/setupAppEvents'
import setupIPCEvents from './main/setupIPCEvents'

// Module

const appData = {
  appWindow: undefined,
  store:     undefined,
}

generateStore(appData)
setupAppEvents(appData)
setupIPCEvents(appData)
