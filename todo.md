# To-Do

* rename `CHXBOT_FORMAT__` constants to `CHXBOT_VARIANT__`
* make `chxBot` and `chxbot` names consistently cased
* clean up/refactor main process event handlers into modules referenced via map
* check all dispatchProps and stateProps files for unused props
* update settings styles to use theme padding
* investigate if BackgroundBlur classes can be assigned during render instead of `componentDidMount()`
* fix UIDisabler, turn it into a modal?
* fix automation flows downloads list filters not resetting when skill changes
* make modals know how to close themselves properly - currently manually passing in a close function from components using modal
* add clear button to filter fields
* check all styles theme spacing usage for consistency
* add debugging section to readme explaining action logs
* investigate `class implemented in both...` main process console errors
* is settings component using `hideModalComplete` dispatch prop?
* rename `background` refs to `backgroundBlur` to disambiguate
  instances of `Background` vs `BackgroundBlur`
* rename `Background` component to be less ambiguous vs `BackgroundBlur`
* replace hideModal action with hideModalComplete?
* check non-component file import paths for aliases
* add ace code editor theme setting
* add chxbot version to skill editor
    * scan downloads to find version number
    * when changed, store in downloads
    * can there be multiple chxbot versions?
* add bookmarks to skill list
* add raw data interface for skill files
* add undo/redo functionality
* host fonts locally
* add dark mode toggle
* make skills drawer resizable
    * https://stackoverflow.com/questions/49469834/recommended-way-to-have-drawer-resizable
* make entry comparison code make shorter filenames appear before filenames that are the same with extra chars
* use react-window in skills list for better efficiency
* convert main files with directories into index files
* organize action files by corresponding state part?
* organize misc ./src/\*.js files that aren't organized
    * constants into ./constants/
    * master constants file?
        * in addition to sub files?
        * instead of sub files?
* save settings to disk so you aren't asked every time
    * best place to store? local user settings?
