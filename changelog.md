# Changelog

## 0.4.0

* Add undo/redo functionality

## 0.3.0

* Persist user settings

## 0.2.0

* Added console

## 0.1.0

* Added skill list and skill viewer
