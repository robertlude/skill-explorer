const HtmlWebpackPlugin = require('html-webpack-plugin')
    , path              = require('path')
    , webpack           = require('webpack')

    , commonPlugins = [
        new webpack.ProvidePlugin({
          debugValues: path.resolve(path.join(__dirname, 'src/debugValues')),
        })
      ]
    , commonResolve = {
        alias: {
          '@': path.resolve(__dirname, 'src'),
          c: path.resolve(__dirname, 'src/components'),
        }
      }

module.exports = [
  {
    entry:   './src/main.js',
    mode:    'development',
    resolve: commonResolve,
    target:  'electron-main',
    module: {
      rules: [
        {
          test:    /\.js/,
          include: /src/,
          use: {
            loader: 'babel-loader',
          },
        },
      ],
    },
    output: {
      filename: 'main.js',
      path:     __dirname + '/dist',
    },
    plugins: commonPlugins,
  },
  {
    entry:   './src/renderer.js',
    mode:    'development',
    resolve: commonResolve,
    target:  'electron-renderer',
    module: {
      rules: [
        {
          test:    /\.js/,
          include: /src/,
          use: {
            loader: 'babel-loader',
          },
        },
      ],
    },
    output: {
      filename: 'renderer.js',
      path:     __dirname + '/dist',
    },
    plugins: [
      ...commonPlugins,
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
    ],
  },
]
