# Console Documentation

The console feature allows you to run code against internal data. This can be
useful if you have a question about *all* skills.

## Warnings

I don't know why, but `lodash` is exposed to the sandbox. I went to include it
manually, and found I didn't need to. That's convenient, but now I wonder about
the sandbox in general. Take care to always define variables.

There is also no error feedback (yet). If your code is bad, you just won't get
any output.

Finally, and this is actually an intended feature, **if you modify one or more
skills via code, those changes will reflect in the app as a whole,** though, you
may need to open a different skill and reopen the one you're looking at to see
the changes. Will fix that in the future.

## Available Values

| Value         | Description                                                 |
| ------------- | ----------------------------------------------------------- |
| `this.skills` | A flat list of skill objects                                |
| `console`     | A fake console for logging output. Currently supports `log` |
